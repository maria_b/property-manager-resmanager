/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.models;

import com.models.Property;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Maria
 */
public class PropertyTest {
    
    public PropertyTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getInsurance method, of class Property.
     */
    @Test
    public void testGetInsurance() {
        System.out.println("getInsurance");
        Property instance = new Property(1,"House","456 Einstein Street",1,1,"2000","350","800",4,"345000",25);
        String expResult = "800";
        String result = instance.getInsurance();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setInsurance method, of class Property.
     */
    @Test
    public void testSetInsurance() {
        System.out.println("setInsurance");
        String insurance = "600";
        Property instance = new Property(1,"House","456 Einstein Street",1,1,"2000","350","800",4,"345000",25);
        instance.setInsurance(insurance);
        assertEquals(insurance, instance.getInsurance());
    }

    /**
     * Test of getTotalUnits method, of class Property.
     */
    @Test
    public void testGetTotalUnits() {
        System.out.println("getTotalUnits");
        Property instance = new Property(1,"House","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        int expResult = 1;
        int result = instance.getTotalUnits();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setTotalUnits method, of class Property.
     */
    @Test
    public void testSetTotalUnits() {
        System.out.println("setTotalUnits");
        int totalUnits = 5;
        Property instance = new Property(1,"Plex","456 Einstein Street",8,0,"2000","350","800",4,"345000",25);
        instance.setTotalUnits(totalUnits);
        assertEquals(totalUnits, instance.getTotalUnits());
    }

    /**
     * Test of getEmptyUnits method, of class Property.
     */
    @Test
    public void testGetEmptyUnits() {
        System.out.println("getEmptyUnits");
        Property instance = new Property(1,"House","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        int expResult = 0;
        int result = instance.getEmptyUnits();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setEmptyUnits method, of class Property.
     */
    @Test
    public void testSetEmptyUnits() {
        System.out.println("setEmptyUnits");
        int emptyUnits = 3;
        Property instance = new Property(1,"Plex","456 Einstein Street",8,6,"2000","350","800",4,"345000",25);
        instance.setEmptyUnits(emptyUnits);
        assertEquals(emptyUnits, instance.getEmptyUnits());
        
    }

    /**
     * Test of getPropertyId method, of class Property.
     */
    @Test
    public void testGetPropertyId() {
        System.out.println("getPropertyId");
        Property instance = new Property(8,"House","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        int expResult = 8;
        int result = instance.getPropertyId();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setPropertyId method, of class Property.
     */
    @Test
    public void testSetPropertyId() {
        System.out.println("setPropertyId");
        int propertyId = 10;
        Property instance = new Property(8,"House","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        instance.setPropertyId(propertyId);
        assertEquals(propertyId, instance.getPropertyId());
    }

    /**
     * Test of getPropertyType method, of class Property.
     */
    @Test
    public void testGetPropertyType() {
        System.out.println("getPropertyType");
        Property instance = new Property(1,"Condo","456 Einstein Street",1,0,"2000","350","800",4,"345000",0);
        String expResult = "Condo";
        String result = instance.getPropertyType();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setPropertyType method, of class Property.
     */
    @Test
    public void testSetPropertyType() {
        System.out.println("setPropertyType");
        String propertyType = "Condo";
        Property instance = new Property(8,"House","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        instance.setPropertyType(propertyType);
        assertEquals(propertyType, instance.getPropertyType());
    }

    /**
     * Test of getPropertyAddr method, of class Property.
     */
    @Test
    public void testGetPropertyAddr() {
        System.out.println("getPropertyAddr");
        Property instance = new Property(1,"Condo","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        String expResult = "456 Einstein Street";
        String result = instance.getPropertyAddr();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setPropertyAddr method, of class Property.
     */
    @Test
    public void testSetPropertyAddr() {
        System.out.println("setPropertyAddr");
        String propertyAddr = "456 McDonald Street";
        Property instance = new Property(1,"Condo","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        instance.setPropertyAddr(propertyAddr);
        assertEquals(propertyAddr, instance.getPropertyAddr());
    }

    /**
     * Test of getCityTax method, of class Property.
     */
    @Test
    public void testGetCityTax() {
        System.out.println("getCityTax");
        Property instance = new Property(1,"Condo","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        String expResult = "2000";
        String result = instance.getCityTax();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCityTax method, of class Property.
     */
    @Test
    public void testSetCityTax() {
        System.out.println("setCityTax");
        String cityTax = "3000";
        Property instance = new Property(1,"Condo","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        instance.setCityTax(cityTax);
        assertEquals(cityTax, instance.getCityTax());
    }

    /**
     * Test of getSchoolTax method, of class Property.
     */
    @Test
    public void testGetSchoolTax() {
        System.out.println("getSchoolTax");
        Property instance = new Property(1,"Condo","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        String expResult = "350";
        String result = instance.getSchoolTax();
        assertEquals(expResult, result);

    }

    /**
     * Test of setSchoolTax method, of class Property.
     */
    @Test
    public void testSetSchoolTax() {
        System.out.println("setSchoolTax");
        String schoolTax = "100";
        Property instance = new Property(1,"Condo","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        instance.setSchoolTax(schoolTax);
        assertEquals(schoolTax, instance.getSchoolTax());
    }

    /**
     * Test of getBankId method, of class Property.
     */
    @Test
    public void testGetBankId() {
        System.out.println("getBankId");
        Property instance = new Property(1,"Condo","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        int expResult = 4;
        int result = instance.getBankId();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setBankId method, of class Property.
     */
    @Test
    public void testSetBankId() {
        System.out.println("setBankId");
        int bankId = 7;
        Property instance = new Property(1,"Condo","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        instance.setBankId(bankId);
        assertEquals(bankId, instance.getBankId());
    }

    /**
     * Test of getMortgageAmount method, of class Property.
     */
    @Test
    public void testGetMortgageAmount() {
        System.out.println("getMortgageAmount");
        Property instance = new Property(1,"Condo","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        String expResult = "345000";
        String result = instance.getMortgageAmount();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMortgageAmount method, of class Property.
     */
    @Test
    public void testSetMortgageAmount() {
        System.out.println("setMortgageAmount");
        String mortgageAmount = "400000";
        Property instance = new Property(1,"Condo","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        instance.setMortgageAmount(mortgageAmount);
        assertEquals(mortgageAmount,  instance.getMortgageAmount());
    }

    /**
     * Test of getLoanLength method, of class Property.
     */
    @Test
    public void testGetLoanLength() {
        System.out.println("getLoanLength");
        Property instance = new Property(1,"Condo","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        int expResult = 25;
        int result = instance.getLoanLength();
        assertEquals(expResult, result);
    }

    /**
     * Test of setLoanLength method, of class Property.
     */
    @Test
    public void testSetLoanLength() {
        System.out.println("setLoanLength");
        int loanLength = 30;
        Property instance = new Property(1,"House","456 Einstein Street",1,0,"2000","350","800",4,"345000",25);
        instance.setLoanLength(loanLength);
        assertEquals(loanLength,  instance.getLoanLength());
    }


    /**
     * Test of calculateMontlyMortgage method, of class Property.
     */
    @Test
    public void testCalculateMontlyMortgage() {
        System.out.println("calculateMontlyMortgage");
        double mortgageRate = 3.00;
        int mortgageAmount = 400000;
        int loanLength = 25;
        double expResult = 1896.85;
        double result = Property.calculateMontlyMortgage(mortgageRate, mortgageAmount, loanLength);
        assertEquals(expResult, result, 0.0);
      
    }
    
}
