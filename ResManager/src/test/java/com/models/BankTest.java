/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.models;

import com.models.Bank;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Maria
 */
public class BankTest {
    
    public BankTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    
   
    /**
     * Test of getBankName method, of class Bank.
     */
    @org.junit.jupiter.api.Test
    public void testGetBankName() {
        System.out.println("getBankName");
        Bank instance = new Bank("TD","0.56",2);
        String expResult = "TD";
        String result = instance.getBankName();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setBankName method, of class Bank.
     */
    @org.junit.jupiter.api.Test
    public void testSetBankName() {
        System.out.println("setBankName");
        String bankName = "CIBC";
        Bank instance = new Bank("TD","0.56",2);
        instance.setBankName(bankName);
        assertEquals(bankName, instance.getBankName());
    }

    /**
     * Test of getInterestRate method, of class Bank.
     */
    @org.junit.jupiter.api.Test
    public void testGetInterestRate() {
        System.out.println("getInterestRate");
        Bank instance = new Bank("CIBC","0.32",4);
        String expResult = "0.32";
        String result = instance.getInterestRate();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setInterestRate method, of class Bank.
     */
    @org.junit.jupiter.api.Test
    public void testSetInterestRate() {
        System.out.println("setInterestRate");
        String interestRate = "0.38";
        Bank instance = new Bank("CIBC","0.32",4);
        instance.setInterestRate(interestRate);
        assertEquals(interestRate, instance.getInterestRate());
    }

    /**
     * Test of getBankId method, of class Bank.
     */
    @org.junit.jupiter.api.Test
    public void testGetBankId() {
        System.out.println("getBankId");
        Bank instance = new Bank("RBC","0.67",1);
        int expResult = 1;
        int result = instance.getBankId();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setBankId method, of class Bank.
     */
    @org.junit.jupiter.api.Test
    public void testSetBankId() {
        System.out.println("setBankId");
        int bankId = 9;
        Bank instance = new Bank("RBC","0.67",1);
        instance.setBankId(bankId);
        assertEquals(bankId, instance.getBankId());
    }

    
}
