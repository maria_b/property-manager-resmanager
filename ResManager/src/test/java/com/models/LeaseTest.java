/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.models;

import com.models.Lease;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Maria
 */
public class LeaseTest {
    
    public LeaseTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getPath method, of class Lease.
     */
    @Test
    public void testGetPath() {
        System.out.println("getPath");
        Lease instance = new Lease("123456789","C://Documents//lease.pdf");
        String expResult = "C://Documents//lease.pdf";
        String result = instance.getPath();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setPath method, of class Lease.
     */
    @Test
    public void testSetPath() {
        System.out.println("setPath");
        String path = "C://Documents//Newlease.pdf";
        Lease instance = new Lease("123456789","C://Documents//lease.pdf");
        instance.setPath(path);
        assertEquals(path,  instance.getPath());
    }

    /**
     * Test of getSIN method, of class Lease.
     */
    @Test
    public void testGetSIN() {
        System.out.println("getSIN");
        Lease instance = new Lease("123456789","C://Documents//lease.pdf");
        String expResult ="123456789";
        String result = instance.getSIN();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setSIN method, of class Lease.
     */
    @Test
    public void testSetSIN() {
        System.out.println("setSIN");
        String SIN = "123456788";
        Lease instance = new Lease("123456789","C://Documents//lease.pdf");
        instance.setSIN(SIN);
        assertEquals(SIN, instance.getSIN());
    }

  
    
    
}
