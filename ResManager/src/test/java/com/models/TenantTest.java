/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.models;

import com.models.Tenant;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Maria
 */
public class TenantTest {
    
    public TenantTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getSin method, of class Tenant.
     */
    @Test
    public void testGetSin() {
        System.out.println("getSin");
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        String expResult = "333444555";
        String result = instance.getSin();
        assertEquals(expResult, result);
    }

    /**
     * Test of setSin method, of class Tenant.
     */
    @Test
    public void testSetSin() {
        System.out.println("setSin");
        String sin = "123456789";
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        instance.setSin(sin);
        assertEquals(sin, instance.getSin());
    }

    /**
     * Test of getHouseid method, of class Tenant.
     */
    @Test
    public void testGetHouseid() {
        System.out.println("getHouseid");
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        int expResult = 1;
        int result = instance.getHouseid();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setHouseid method, of class Tenant.
     */
    @Test
    public void testSetHouseid() {
        System.out.println("setHouseid");
        int houseid = 45;
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        instance.setHouseid(houseid);
        assertEquals(houseid, instance.getHouseid());
    }

    /**
     * Test of getTenantName method, of class Tenant.
     */
    @Test
    public void testGetTenantName() {
        System.out.println("getTenantName");
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        String expResult = "Flash Jones";
        String result = instance.getTenantName();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setTenantName method, of class Tenant.
     */
    @Test
    public void testSetTenantName() {
        System.out.println("setTenantName");
        String tenantName = "Flash Jin";
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        instance.setTenantName(tenantName);
        assertEquals(tenantName, instance.getTenantName());
    }

    /**
     * Test of getPhoneNumber method, of class Tenant.
     */
    @Test
    public void testGetPhoneNumber() {
        System.out.println("getPhoneNumber");
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        String expResult = "7894567890";
        String result = instance.getPhoneNumber();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setPhoneNumber method, of class Tenant.
     */
    @Test
    public void testSetPhoneNumber() {
        System.out.println("setPhoneNumber");
        String phoneNumber = "1234567891";
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        instance.setPhoneNumber(phoneNumber);
        assertEquals(phoneNumber, instance.getPhoneNumber());
    }

    /**
     * Test of getEmail method, of class Tenant.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
         Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        String expResult = "j@email.com";
        String result = instance.getEmail();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setEmail method, of class Tenant.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        String email = "hghg@email.com";
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        instance.setEmail(email);
        assertEquals(email,  instance.getEmail());
    }

    /**
     * Test of getMonthlyRent method, of class Tenant.
     */
    @Test
    public void testGetMonthlyRent() {
        System.out.println("getMonthlyRent");
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        String expResult = "2400";
        String result = instance.getMonthlyRent();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setMonthlyRent method, of class Tenant.
     */
    @Test
    public void testSetMonthlyRent() {
        System.out.println("setMonthlyRent");
        String monthlyRent = "3000";
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        instance.setMonthlyRent(monthlyRent);
        assertEquals(monthlyRent, instance.getMonthlyRent());
    }

    /**
     * Test of getUtilitiesCost method, of class Tenant.
     */
    @Test
    public void testGetUtilitiesCost() {
        System.out.println("getUtilitiesCost");
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        String expResult = "300";
        String result = instance.getUtilitiesCost();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setUtilitiesCost method, of class Tenant.
     */
    @Test
    public void testSetUtilitiesCost() {
        System.out.println("setUtilitiesCost");
        String utilitiesCost = "450";
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        instance.setUtilitiesCost(utilitiesCost);
        assertEquals(utilitiesCost, instance.getUtilitiesCost());
       
    }

    /**
     * Test of getAddress method, of class Tenant.
     */
    @Test
    public void testGetAddress() {
        System.out.println("getAddress");
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        String expResult = "42 Sakura Drive";
        String result = instance.getAddress();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setAddress method, of class Tenant.
     */
    @Test
    public void testSetAddress() {
        System.out.println("setAddress");
        String address = "789 Sakura Drive";
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        instance.setAddress(address);
        assertEquals(address, instance.getAddress());
    }

    /**
     * Test of getPaidRent method, of class Tenant.
     */
    @Test
    public void testGetPaidRent() {
        System.out.println("getPaidRent");
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        String expResult = "True";
        String result = instance.getPaidRent();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setPaidRent method, of class Tenant.
     */
    @Test
    public void testSetPaidRent() {
        System.out.println("setPaidRent");
        String paidRent = "False";
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        instance.setPaidRent(paidRent);
        assertEquals(paidRent, instance.getPaidRent());
    }

    /**
     * Test of getLeaseRenewed method, of class Tenant.
     */
    @Test
    public void testGetLeaseRenewed() {
        System.out.println("getLeaseRenewed");
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        String expResult = "True";
        String result = instance.getLeaseRenewed();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setLeaseRenewed method, of class Tenant.
     */
    @Test
    public void testSetLeaseRenewed() {
        System.out.println("setLeaseRenewed");
        String leaseRenewed = "False";
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        instance.setLeaseRenewed(leaseRenewed);
        assertEquals(leaseRenewed,  instance.getLeaseRenewed());
    }

    /**
     * Test of getRentOverdue method, of class Tenant.
     */
    @Test
    public void testGetRentOverdue() {
        System.out.println("getRentOverdue");
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        String expResult = "False";
        String result = instance.getRentOverdue();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setRentOverdue method, of class Tenant.
     */
    @Test
    public void testSetRentOverdue() {
        System.out.println("setRentOverdue");
        String rentOverdue = "True";
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        instance.setRentOverdue(rentOverdue);
        assertEquals(rentOverdue, instance.getRentOverdue());
    }

    /**
     * Test of getFlag method, of class Tenant.
     */
    @Test
    public void testGetFlag() {
        System.out.println("getFlag");
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        String expResult = "False";
        String result = instance.getFlag();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setFlag method, of class Tenant.
     */
    @Test
    public void testSetFlag() {
        System.out.println("setFlag");
        String flag = "True";
        Tenant instance = new Tenant("333444555",1,"Flash Jones","7894567890",
                 "j@email.com","2400","300","42 Sakura Drive","True","True","False","False");
        instance.setFlag(flag);
        assertEquals(flag,  instance.getFlag());
    }

   
    
}
