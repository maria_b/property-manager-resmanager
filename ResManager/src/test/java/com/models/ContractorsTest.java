/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.models;

import com.models.Contractors;
import java.sql.Date;
import java.text.SimpleDateFormat;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Maria
 */
public class ContractorsTest {
    
    public ContractorsTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getContractorName method, of class Contractors.
     */
    @Test
    public void testGetContractorName() {
        
        Date date=Date.valueOf("2021-03-04");
        System.out.println("getContractorName");
        Contractors instance = new Contractors("James","James Co","1234567890","Painting","400",date);
        String expResult = "James";
        String result = instance.getContractorName();
        assertEquals(expResult, result);
        
        
    }

    /**
     * Test of setContractorName method, of class Contractors.
     */
    @Test
    public void testSetContractorName() {
        Date date=Date.valueOf("2021-03-04");
        System.out.println("setContractorName");
        String contractorName = "Hugo";
        Contractors instance =  new Contractors("James","James Co","1234567890","Painting","400",date);
        instance.setContractorName(contractorName);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals("Hugo", instance.getContractorName());
    }

    /**
     * Test of getCompanyName method, of class Contractors.
     */
    @Test
    public void testGetCompanyName() {
        Date date=Date.valueOf("2021-03-04");
        System.out.println("getCompanyName");
        Contractors instance = new Contractors("James","James Co","1234567890","Painting","400",date);
        String expResult = "James Co";
        String result = instance.getCompanyName();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setCompanyName method, of class Contractors.
     */
    @Test
    public void testSetCompanyName() {
        Date date=Date.valueOf("2021-03-04");
        System.out.println("setCompanyName");
        String companyName = "James Company";
        Contractors instance = new Contractors("James","James Co","1234567890","Painting","400",date);
        instance.setCompanyName(companyName);
        assertEquals("James Company", instance.getCompanyName());
    }

    /**
     * Test of getPhoneNumber method, of class Contractors.
     */
    @Test
    public void testGetPhoneNumber() {
        Date date=Date.valueOf("2021-03-04");
        System.out.println("getPhoneNumber");
        Contractors instance = new Contractors("James","James Co","1234567890","Painting","400",date);
        String expResult = "1234567890";
        String result = instance.getPhoneNumber();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setPhoneNumber method, of class Contractors.
     */
    @Test
    public void testSetPhoneNumber() {
        Date date=Date.valueOf("2021-03-04");
        System.out.println("setPhoneNumber");
        String phoneNumber = "1234567111";
        Contractors instance = new Contractors("James","James Co","1234567890","Painting","400",date);
        instance.setPhoneNumber(phoneNumber);
        assertEquals(phoneNumber, instance.getPhoneNumber());
    }

    /**
     * Test of getTypeOfMaintenace method, of class Contractors.
     */
    @Test
    public void testGetTypeOfMaintenace() {
        Date date=Date.valueOf("2021-03-04");
        System.out.println("getTypeOfMaintenace");
        Contractors instance = new Contractors("James","James Co","1234567890","Painting","400",date);
        String expResult = "Painting";
        String result = instance.getTypeOfMaintenace();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setTypeOfMaintenace method, of class Contractors.
     */
    @Test
    public void testSetTypeOfMaintenace() {
        Date date=Date.valueOf("2021-03-04");
        System.out.println("setTypeOfMaintenace");
        String typeOfMaintenace = "Washing";
        Contractors instance =new Contractors("James","James Co","1234567890","Painting","400",date);
        instance.setTypeOfMaintenace(typeOfMaintenace);
        assertEquals(typeOfMaintenace, instance.getTypeOfMaintenace());
    }

    /**
     * Test of getMaintenanceFee method, of class Contractors.
     */
    @Test
    public void testGetMaintenanceFee() {
        Date date=Date.valueOf("2021-03-04");
        System.out.println("getMaintenanceFee");
        Contractors instance = new Contractors("James","James Co","1234567890","Painting","400",date);
        String expResult = "400";
        String result = instance.getMaintenanceFee();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setMaintenanceFee method, of class Contractors.
     */
    @Test
    public void testSetMaintenanceFee() {
        Date date=Date.valueOf("2021-03-04");
        System.out.println("setMaintenanceFee");
        String maintenanceFee = "600";
        Contractors instance = new Contractors("James","James Co","1234567890","Painting","400",date);
        instance.setMaintenanceFee(maintenanceFee);
        assertEquals(maintenanceFee, instance.getMaintenanceFee());
    }

    /**
     * Test of getDate method, of class Contractors.
     */
    @Test
    public void testGetDate() {
        Date date=Date.valueOf("2021-03-04");
        System.out.println("getDate");
        Contractors instance = new Contractors("James","James Co","1234567890","Painting","400",date);
        Date expResult = date;
        Date result = instance.getDate();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setDate method, of class Contractors.
     */
    @Test
    public void testSetDate() {
        Date date=Date.valueOf("2021-03-04");
        System.out.println("setDate");
        Date dateToResult = Date.valueOf("2021-03-06");;
        Contractors instance = new Contractors("James","James Co","1234567890","Painting","400",date);
        instance.setDate(dateToResult);
        assertEquals(dateToResult, instance.getDate());
    }


    
}
