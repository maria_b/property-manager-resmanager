/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.models;

/**
 * This class creates a Plex object that inherits from Property
 *
 * @author Maria Barba
 * @version 1.0
 * @since 2021-05-01
 */
public class Plex extends Property {

    /**
     * Constructor for Plex that takes as input all values from Property.
     *
     * @param propertyId
     * @param propertyType
     * @param propertyAddr
     * @param totalUnits
     * @param emptyUnits
     * @param cityTax
     * @param schoolTax
     * @param insurance
     * @param bankId
     * @param mortgageAmount
     * @param loanLength
     */
    public Plex(int propertyId, String propertyType, String propertyAddr, int totalUnits, int emptyUnits, String cityTax, String schoolTax, String insurance, int bankId, String mortgageAmount, int loanLength) {
        super(propertyId, propertyType, propertyAddr, totalUnits, emptyUnits, cityTax, schoolTax, insurance, bankId, mortgageAmount, loanLength);
    }

}
