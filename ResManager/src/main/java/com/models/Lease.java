/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.models;

/**
 * This class creates a Lease object
 *
 * @author Maria Barba
 * @version 1.0
 * @since 2021-05-01
 *
 *
 */
public class Lease {

    private String path;
    private String SIN;

    /**
     * Constructor for Lease object
     *
     * @param path String
     * @param SIN String
     */
    public Lease(String SIN, String path) {
        this.SIN = SIN;
        this.path = path;
        
    }

    /**
     * Return path of the lease
     *
     * @return String path
     */
    public String getPath() {
        return path;
    }

    /**
     * Setter for path
     *
     * @param path String
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Getter for SIN of Lease
     *
     * @return SIN String
     */
    public String getSIN() {
        return SIN;
    }

    /**
     * Setter for SIN
     *
     * @param SIN String
     */
    public void setSIN(String SIN) {
        this.SIN = SIN;
    }

    /**
     * To String for lease
     *
     * @return String
     */
    @Override
    public String toString() {
        return "Lease{" + "path=" + path + ", SIN=" + SIN + '}';
    }
}
