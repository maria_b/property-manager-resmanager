/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.models;

/**
 * This class creates a Bank object used in the view BankView
 * @author Maria Barba
 * @version 1.0
 * @since 2021-04-20
 */
public class Bank {
    private String bankName;
    private String interestRate;
    private int bankId;
    
    /**
    * The constructor for the Bank class.
    * @param bankName
    * @param interestRate
    * @param bankId 
    */
    public Bank(String bankName, String interestRate, int bankId) {
        this.bankName = bankName;
        this.interestRate = interestRate;
        this.bankId = bankId;
    }
    /**
     * getter for the bankName of a Bank object
     * @return String 
     */

    public String getBankName() {
        return bankName;
    }
    /**
     * Setter for bankName of the Bank object
     * @param bankName String
     * @return void
     */

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
    /**
     * Getter for interest rate of the Bank object.
     * @return String interestRate
     */
    public String getInterestRate() {
        return interestRate;
    }
    /**
     * Setter for interest rate of a Bank object.
     * @param interestRate String
     */
    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }
    /**
     * Getter for bank id of a Bank object.
     * @return int bankId
     */
    public int getBankId() {
        return bankId;
    }
    /**
     * Setter for bank id of the Bank object.
     * @param bankId int
     */
    public void setBankId(int bankId) {
        this.bankId = bankId;
    }
    /**
     * ToString method 
     * @return String 
     */
    @Override
    public String toString() {
        return "Bank{" + "bankName=" + bankName + ", interestRate=" + interestRate + ", bankId=" + bankId + '}';
    }
    
    
}
