/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.models;

import java.sql.Date;

/**
 This class creates a Contractors object used in the view ContractorsView
 * @author Maria Barba
 * @version 1.0
 * @since 2021-04-20
 */
public class Contractors {

    private String contractorName;
    private String companyName;
    private String phoneNumber;
    private String typeOfMaintenace;
    private String maintenanceFee;
    private Date date;

    /**
     * The constructor for the Contractors class.
     * @param contractorName String
     * @param companyName String
     * @param phoneNumber String
     * @param typeOfMaintenace String
     * @param maintenanceFee String
     * @param date Date
     */
    public Contractors(String contractorName, String companyName, String phoneNumber, String typeOfMaintenace, String maintenanceFee, Date date) {
        this.contractorName = contractorName;
        this.companyName = companyName;
        this.phoneNumber = phoneNumber;
        this.typeOfMaintenace = typeOfMaintenace;
        this.maintenanceFee = maintenanceFee;
        this.date = date;
    }

    /**
     * Getter for contractors name 
     * @return String contractorName
     */
    public String getContractorName() {
        return contractorName;
    }

    /**
     * Setter for contractors name
     * @param contractorName String
     */
    public void setContractorName(String contractorName) {
        this.contractorName = contractorName;
    }

    /**
     * Getter for company name
     * @return String companyName
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Setter for company name 
     * @param companyName String
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * Getter for phone number
     * @return String phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Setter for phone number 
     * @param phoneNumber String
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Getter for type of maintenance
     * @return String typeOfMaintenace
     */
    public String getTypeOfMaintenace() {
        return typeOfMaintenace;
    }

    /**
     * Setter for type of maintenance 
     * @param typeOfMaintenace String
     */
    public void setTypeOfMaintenace(String typeOfMaintenace) {
        this.typeOfMaintenace = typeOfMaintenace;
    }

    /**
     * Getter for maintenance fee
     * @return String maintenanceFee
     */
    public String getMaintenanceFee() {
        return maintenanceFee;
    }

    /**
     * Setter for maintenanceFee 
     * @param maintenanceFee String
     */
    public void setMaintenanceFee(String maintenanceFee) {
        this.maintenanceFee = maintenanceFee;
    }

    /**
     * Getter for maintenance date
     * @return Date date
     */
    public Date getDate() {
        return date;
    }

    /**
     *
     * @param date
     */
    public void setDate(Date date) {
        this.date = date;
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString() {
      return ("Contractors name : " + this.contractorName + " company name: " + this.companyName + " phone: " + this.phoneNumber +  " type : " + this.typeOfMaintenace + " fee : "+ this.maintenanceFee +  " date : " + this.date );  
    }

}
