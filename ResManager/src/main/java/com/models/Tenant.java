/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.models;

import java.util.Observable;

/**
 * This class creates a Tenant object used in TenantView
 *
 * @author Maria Barba
 * @version 1.0
 * @since 2021-05-01
 */
public class Tenant {

    private String sin;
    private int houseid;
    private String tenantName;
    private String phoneNumber;
    private String email;
    private String monthlyRent;
    private String utilitiesCost;
    private String address;
    private String paidRent;
    private String leaseRenewed;
    private String rentOverdue;
    private String flag;

    /**
     * Constructor for Tenant
     *
     * @param sin String
     * @param houseid int
     * @param tenantName String
     * @param phoneNumber String
     * @param email String
     * @param monthlyRent String
     * @param utilitiesCost String
     * @param address String
     * @param paidRent String
     * @param leaseRenewed String
     * @param rentOverdue String
     * @param flag String
     */
    public Tenant(String sin, int houseid, String tenantName, String phoneNumber, String email, String monthlyRent, String utilitiesCost, String address, String paidRent, String leaseRenewed, String rentOverdue, String flag) {
        this.sin = sin;
        this.houseid = houseid;
        this.tenantName = tenantName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.monthlyRent = monthlyRent;
        this.utilitiesCost = utilitiesCost;
        this.address = address;
        this.paidRent = paidRent;
        this.leaseRenewed = leaseRenewed;
        this.rentOverdue = rentOverdue;
        this.flag = flag;
    }

    /**
     * Getter for sin
     *
     * @return sin String
     */
    public String getSin() {
        return sin;
    }

    /**
     * Setter for sin
     *
     * @param sin String
     */
    public void setSin(String sin) {
        this.sin = sin;
    }

    /**
     * Getter for houseid
     *
     * @return houseid int
     */
    public int getHouseid() {
        return houseid;
    }

    /**
     * Setter for houseid
     *
     * @param houseid String
     */
    public void setHouseid(int houseid) {
        this.houseid = houseid;
    }

    /**
     * Getter for tenantName
     *
     * @return tenant String
     */
    public String getTenantName() {
        return tenantName;
    }

    /**
     * Setter for tenant name
     *
     * @param tenantName String
     */
    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    /**
     * Getter for phone number
     *
     * @return phoneNumber String
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Setter for phoneNumber
     *
     * @param phoneNumber String
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Getter for email
     *
     * @return email String
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter for email
     *
     * @param email String
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter for montlyRent
     *
     * @return montlyRent String
     */
    public String getMonthlyRent() {
        return monthlyRent;
    }

    /**
     * Setter for monthlyRent
     *
     * @param monthlyRent String
     */
    public void setMonthlyRent(String monthlyRent) {
        this.monthlyRent = monthlyRent;
    }

    /**
     * Getter for utilitiesCost
     *
     * @return utilitiesCost String
     */
    public String getUtilitiesCost() {
        return utilitiesCost;
    }

    /**
     * Setter for utilitiesCost
     *
     * @param utilitiesCost String
     */
    public void setUtilitiesCost(String utilitiesCost) {
        this.utilitiesCost = utilitiesCost;
    }

    /**
     * Getter for address
     *
     * @return address String
     */
    public String getAddress() {
        return address;
    }

    /**
     * Setter for address
     *
     * @param address String
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Getter for paidRent
     *
     * @return paidRent String
     */
    public String getPaidRent() {
        return paidRent;
    }

    /**
     * Setter for paidRent
     *
     * @param paidRent String
     */
    public void setPaidRent(String paidRent) {
        this.paidRent = paidRent;
    }

    /**
     * Getter for leaseRenewed
     *
     * @return leaseRenewed String
     */
    public String getLeaseRenewed() {
        return leaseRenewed;
    }

    /**
     * Setter for leaseRenewed
     *
     * @param leaseRenewed String
     */
    public void setLeaseRenewed(String leaseRenewed) {
        this.leaseRenewed = leaseRenewed;
    }

    /**
     * Getter for rentOverdue
     *
     * @return rentOverdue String
     */
    public String getRentOverdue() {
        return rentOverdue;
    }

    /**
     * Setter for rentOverdue
     *
     * @param rentOverdue String
     */
    public void setRentOverdue(String rentOverdue) {
        this.rentOverdue = rentOverdue;
    }

    /**
     * Getter for flag
     *
     * @return flag String
     */
    public String getFlag() {
        return flag;
    }

    /**
     * Setter for flag
     *
     * @param flag String
     */
    public void setFlag(String flag) {
        this.flag = flag;
    }

    /**
     * toString method
     *
     * @return String
     */
    @Override
    public String toString() {
        return "Tenant{" + "sin=" + sin + ", houseid=" + houseid + ", tenantName=" + tenantName + ", phoneNumber=" + phoneNumber + ", email=" + email + ", monthlyRent=" + monthlyRent + ", utilitiesCost=" + utilitiesCost + ", address=" + address + ", paidRent=" + paidRent + ", leaseRenewed=" + leaseRenewed + ", rentOverdue=" + rentOverdue + ", flag=" + flag + '}';
    }

}
