/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.models;

/**
* This class creates a Condo object used in the view PropertyView
 * @author Maria Barba
 * @version 1.0
 * @since 2021-05-01
 */
public class Condo extends Property {
    //attribute condoFee that is necessary to create a Condo object
    private String condoFee;
    /**
     * Condo constructor that takes input all fields from Property and the condoFee attribute
     * @param propertyId
     * @param propertyType
     * @param propertyAddr
     * @param totalUnits
     * @param emptyUnits
     * @param cityTax
     * @param schoolTax
     * @param insurance
     * @param bankId
     * @param mortgageAmount
     * @param loanLength
     * @param condoFee 
     */
    public Condo(int propertyId, String propertyType, String propertyAddr, int totalUnits, int emptyUnits, String cityTax, String schoolTax, String insurance, int bankId, String mortgageAmount, int loanLength, String condoFee) {
        super(propertyId, propertyType, propertyAddr, totalUnits, emptyUnits, cityTax, schoolTax, insurance, bankId, mortgageAmount, loanLength);
        this.condoFee = condoFee;
    }
    /**
     * Getter for condoFee
     * @return String condoFee
     */
    public String getCondoFee() {
        return condoFee;
    }
    /**
     * Setter for condoFee
     * @param condoFee String
     */
    public void setCondoFee(String condoFee) {
        this.condoFee = condoFee;
    }

}
