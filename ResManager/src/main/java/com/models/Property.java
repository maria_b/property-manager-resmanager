/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.models;

import java.text.DecimalFormat;

/**
 * This class creates a Proprety object used in PropertyView
 *
 * @author Maria Barba
 * @version 1.0
 * @since 2021-05-01
 */
public class Property {

    private int propertyId;
    private String propertyType;
    private String propertyAddr;
    private int totalUnits;
    private int emptyUnits;
    private String cityTax;
    private String schoolTax;
    private String insurance;
    private int bankId;
    private String mortgageAmount;
    private int loanLength;

    /**
     * Constructor for Property.
     *
     * @param propertyId
     * @param propertyType
     * @param propertyAddr
     * @param totalUnits
     * @param emptyUnits
     * @param cityTax
     * @param schoolTax
     * @param insurance
     * @param bankId
     * @param mortgageAmount
     * @param loanLength
     */
    public Property(int propertyId, String propertyType, String propertyAddr, int totalUnits, int emptyUnits, String cityTax, String schoolTax, String insurance, int bankId, String mortgageAmount, int loanLength) {
        this.propertyId = propertyId;
        this.propertyType = propertyType;
        this.propertyAddr = propertyAddr;
        this.totalUnits = totalUnits;
        this.emptyUnits = emptyUnits;
        this.cityTax = cityTax;
        this.schoolTax = schoolTax;
        this.insurance = insurance;
        this.bankId = bankId;
        this.mortgageAmount = mortgageAmount;
        this.loanLength = loanLength;
    }

    /**
     * Getter for insurance
     *
     * @return insurance String
     */
    public String getInsurance() {
        return insurance;
    }

    /**
     * *
     * Setter for insurance
     *
     * @param insurance String
     */
    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    /**
     * Getter for totalUnits
     *
     * @return totalUnits int
     */
    public int getTotalUnits() {
        return totalUnits;
    }

    /**
     * Setter for totalUnits
     *
     * @param totalUnits int
     */
    public void setTotalUnits(int totalUnits) {
        this.totalUnits = totalUnits;
    }

    /**
     * Getter for emptyUnits
     *
     * @return emptyUnits int
     */
    public int getEmptyUnits() {
        return emptyUnits;
    }

    /**
     * Setter for emptyUnits
     *
     * @param emptyUnits int
     */
    public void setEmptyUnits(int emptyUnits) {
        this.emptyUnits = emptyUnits;
    }

    /**
     * Getter for propertyId
     *
     * @return int
     */
    public int getPropertyId() {
        return propertyId;
    }

    /**
     * Setter for propertyId
     *
     * @param propertyId int
     */
    public void setPropertyId(int propertyId) {
        this.propertyId = propertyId;
    }

    /**
     * Getter for property type
     *
     * @return propertyType String
     */
    public String getPropertyType() {
        return propertyType;
    }

    /**
     * Setter for propertyType
     *
     * @param propertyType String
     */
    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    /**
     * Getter for propertyAddr
     *
     * @return propertyAddr String
     */
    public String getPropertyAddr() {
        return propertyAddr;
    }

    /**
     * Setter for propertyAddr
     *
     * @param propertyAddr String
     */
    public void setPropertyAddr(String propertyAddr) {
        this.propertyAddr = propertyAddr;
    }

    /**
     * Getter for cityTax
     *
     * @return cityTax String
     */
    public String getCityTax() {
        return cityTax;
    }

    /**
     * Setter for cityTax
     *
     * @param cityTax String
     */
    public void setCityTax(String cityTax) {
        this.cityTax = cityTax;
    }

    /**
     * Getter for schoolTax
     *
     * @return schoolTax String
     */
    public String getSchoolTax() {
        return schoolTax;
    }

    /**
     * Setter for schoolTax String
     *
     * @param schoolTax String
     */
    public void setSchoolTax(String schoolTax) {
        this.schoolTax = schoolTax;
    }

    /**
     * Getter for bankId
     *
     * @return bankId int
     */
    public int getBankId() {
        return bankId;
    }

    /**
     * Setter for bankId
     *
     * @param bankId int
     */
    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    /**
     * Getter for mortgageAmount
     *
     * @return mortgageAmount String
     */
    public String getMortgageAmount() {
        return mortgageAmount;
    }

    /**
     * Setter for mortgageAmount
     *
     * @param mortgageAmount String
     */
    public void setMortgageAmount(String mortgageAmount) {
        this.mortgageAmount = mortgageAmount;
    }

    /**
     * Getter for loanLength
     *
     * @return loanLength int
     */
    public int getLoanLength() {
        return loanLength;
    }

    /**
     * Setter for loanLength
     *
     * @param loanLength String
     */
    public void setLoanLength(int loanLength) {
        this.loanLength = loanLength;
    }

    /**
     * ToString method
     *
     * @return
     */
    @Override
    public String toString() {
        return "Property{" + "propertyId=" + propertyId + ", propertyType=" + propertyType + ", propertyAddr=" + propertyAddr + ", totalUnits=" + totalUnits + ", emptyUnits=" + emptyUnits + ", cityTax=" + cityTax + ", schoolTax=" + schoolTax + ", insurance=" + insurance + ", bankId=" + bankId + ", mortgageAmount=" + mortgageAmount + ", loanLength=" + loanLength + '}';
    }

    /**
     * calculateMontlyMortgage takes as input the mortgage rate, mortgage amount
     * and the length of the loan and calculates the monthly mortgage for a
     * property.
     *
     * @param mortgageRate (divided by 100 then by 12 for each month)
     * @param mortgageAmount (what is the sum of the loan)
     * @param loanLength (how long does the person take the loan for)
     * @return
     */
    public static double calculateMontlyMortgage(double mortgageRate, int mortgageAmount, int loanLength) {
        double convertedMortgageRate = mortgageRate / 100; //divide by 100 and then by 12 to get the montly interest rate
        convertedMortgageRate = convertedMortgageRate / 12;
        int loanLengthMonts = loanLength * 12; //find number of months for a certain period of loan
        double montlyLoan = (mortgageAmount * convertedMortgageRate * Math.pow((1 + convertedMortgageRate), loanLengthMonts)) / (Math.pow((1 + convertedMortgageRate), loanLengthMonts) - 1);
        DecimalFormat df = new DecimalFormat("#.##");
        montlyLoan = Double.valueOf(df.format(montlyLoan)); //round the montly loan value to two places
        return montlyLoan;
    }

}
