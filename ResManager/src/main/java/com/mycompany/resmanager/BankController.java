/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.resmanager;

import com.models.Bank;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import com.mycompany.resmanager.App;
import com.mysqlconnection.*;
import java.io.IOException;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.collections.*;
import java.sql.*;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

/**
 * FXML Controller class for BankView
 * @author Maria Barba
 * @version 1.0
 * @since 2021-04-19
 */
public class BankController extends MainController implements Initializable {

    @FXML
    private TableView<Bank> banksTable;
    @FXML
    private TableColumn<Bank, Integer> bankidCol;
    @FXML
    private TableColumn<Bank, String> banknameCol;
    @FXML
    private TableColumn<Bank, String> bankmorCol;
    @FXML
    private Button button_save;
    @FXML
    private Button button_delete;
    @FXML
    private TextField text_bankname;
    @FXML
    private TextField text_bankmor;
    @FXML
    private TextField text_bankid;
    @FXML
    private ComboBox combo_box;
    
    /**
     * Initializes the controller class.
     * initialize method creates an ObservableList<String> with the bank names 
     * for the user to choose from in the combo box and calls the showBank method
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<String> typeBank = FXCollections.observableArrayList("RBC", "TD", "CIBC", "BMO");
        combo_box.setItems(typeBank);
        showBank();
    }
    /**
     * switchToContractors switches to ContractorsView when the contractors button is clicked
     * @throws IOException 
     */
    @FXML
    private void switchToContractors() throws IOException {
        App.setRoot("ContractorsView");
    }
    /**
     * switchToTenants switches to tenantView when the tenants button is clicked
     * @throws IOException 
     */
    @FXML
    private void switchToTenants() throws IOException {
        App.setRoot("TenantView");
    }
    /**
     * switchPropretyView switches to PropretyView when the properties button is clicked
     * @throws IOException 
     */
    @FXML
    private void switchPropretyView() throws IOException {
        App.setRoot("PropretyView");
    }
    /**
     * switchToLeases switches to LeaseView when the leases button is clicked
     * @throws IOException 
     */
    @FXML
    private void switchToLeases() throws IOException {
        App.setRoot("LeaseView");
    }
    /**
     * getBankList returns an ObservableList of the Bank object type from the banks
     * table to be displayed in the TableView upon opening BankView
     * @return ObservableList<Bank> using data from the banks table inside the database
     */
    public ObservableList<Bank> getBankList() {
        ObservableList<Bank> bankList = FXCollections.observableArrayList();
        DatabaseReferrer dbRef= DatabaseReferrer.getInstance();
        Connection conn = dbRef.returnConn(); //calls method to return Connection object
        String query = "SELECT * FROM banks"; // select all data from banks table
        Statement statement;
        ResultSet resultset;

        try {
            statement = conn.createStatement(); //create a statement
            resultset = statement.executeQuery(query); //create a resultset from the statement
            Bank banks;
            while (resultset.next()) {
                banks = new Bank(resultset.getString("bank_name"), resultset.getString("bank_mortgage"), resultset.getInt("bank_id"));
                bankList.add(banks); //add Bank object to observableList
            }
        } catch (Exception exception) {
            exception.printStackTrace();//catch and print the exception if any
        }
        return bankList;
    }
    /**
     * showBank sets the TableView and the columns using the observable list returned from
     * the getBankList method
     */
    public void showBank() {
        ObservableList<Bank> list = getBankList(); //get the BankList
        bankidCol.setCellValueFactory(new PropertyValueFactory<Bank, Integer>("bankId")); //set banks id column
        banknameCol.setCellValueFactory(new PropertyValueFactory<Bank, String>("bankName")); //set bank name column
        bankmorCol.setCellValueFactory(new PropertyValueFactory<Bank, String>("interestRate")); //set interest rate column
        banksTable.setItems(list); //set the banks tbale with the appropriate information
    }
    /**
     * addBankData inserts data written in the text fields by the user into the banks table in the database.
     */
    @FXML
    private void addBankData() {
        //write a query to get data from text fileds of the views and insert into the banks table.
        String query = "INSERT INTO banks VALUES (" + text_bankid.getText() + ",'" + text_bankname.getText() + "','" + text_bankmor.getText() + "')";
        if (!(checkFormFileds()) || !(DataManipulation.countBanks()==4)){
        DataManipulation.executeQuery(query); //execute Query
        }
        showBank(); //display TbaleView data
    }
    
   /**
    * deleteBtn takes as input the Bank object selected in the TableView by the user 
    * and deletes the data from the database using the bank id 
    * @param toDelete Bank
    */
    @FXML
    private void deleteBtn() {
        Bank bankToDelete = banksTable.getSelectionModel().getSelectedItem();
        String query = "DELETE FROM banks WHERE bank_id =" + bankToDelete.getBankId();
        DataManipulation.executeQuery(query);
        showBank();
    }
    /**
     * setComboBoxChoice gets the data from the selected combo box and sets the 
 selected bank to the bank name text field
     * @param event 
     */
    @FXML
    private void setComboBoxChoice(ActionEvent event) {
        String selectedBank = combo_box.getSelectionModel().getSelectedItem().toString();
        text_bankname.setText(selectedBank);
    }
    /**
     * checkFormFileds checks if there are any empty fields and promts the user to fill them
     * also it does number checking for bank id fields and bank mortgage fields.
     * @return result boolean , true for an empty filed or error type
     */
    public boolean  checkFormFileds(){
        //true if empty or error
        boolean result = false;
        if(text_bankname.getText().isEmpty() |  text_bankmor.getText().isEmpty() | 
               text_bankid.getText().isEmpty()){
         result=true;
         super.createErrorBox("Fill all fields before saving to the database !");
        }
        if(!(super.isNumber(text_bankmor.getText())) || !(super.isNumber(text_bankid.getText()))){
        result=true;    
        }
         return result;
    }
}
