/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.resmanager;

import com.models.Property;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import com.models.*;
import com.models.Tenant;

import com.mysqlconnection.*;
import java.util.ArrayList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class for PropetyView
 *
 * @author Maria Barba
 * @version 1.0
 * @since 2021-04-19
 */
public class PropertyController extends MainController implements Initializable {

    @FXML
    private TableView<Property> PropertyView;
    @FXML
    private TableColumn<Property, String> colType;
    @FXML
    private TableColumn<Property, Integer> colId;
    @FXML
    private TableColumn<Property, String> colAddress;
    @FXML
    private TableColumn<Property, Integer> colTotalUnits;
    @FXML
    private TableColumn<Property, Integer> colVacant;
    @FXML
    private TableColumn<Property, String> colCityTax;
    @FXML
    private TableColumn<Property, String> colschoolTax;
    @FXML
    private TableColumn<Property, String> colInsurance;
    @FXML
    private TableColumn<Property, String> colLoanAmount;
    @FXML
    private TableColumn<Property, Integer> colLoanTime;
    @FXML
    private TableColumn<Property, Integer> colBankId;
    @FXML
    private Button btnDelete;
    @FXML
    private TextField tfMontlyMortgage;
    @FXML
    private TextField tfPropId;
    @FXML
    private TableColumn<Condo, String> colCondoFee;
    @FXML
    private TableView<Condo> condoFeecol;
    @FXML
    private Label labelCalculateMortgage;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        showProperties();
    }
   /**
     * switchToContractors switches view to ContractorsView when contractors
     * button is clicked.
     *
     * @throws IOException
     */
    @FXML
    private void switchToContractors() throws IOException {
        App.setRoot("ContractorsView");
    }

    /**
     * switchToTenants changes view to TenantViewwhen the tenants button is
     * clicked.
     *
     * @throws IOException
     */
    @FXML
    private void switchToTenants() throws IOException {
        App.setRoot("TenantView");
    }

    /**
     * switchToInstitutions changes view to BankView when Institutions button is
     * clicked.
     *
     * @throws IOException
     */
    @FXML
    private void switchToInstitutions() throws IOException {
        App.setRoot("BankView");
    }

    /**
     * switchToLeases switches view to LeaseView when the Leases button is
     * clicked.
     *
     * @throws IOException
     */
    @FXML
    private void switchToLeases() throws IOException {
        App.setRoot("LeaseView");
    }

    /**
     * switchToAddProprety changes view to AddingPropretiesView when add button
     * is clicked.
     *
     * @throws IOException
     */
    @FXML
    private void switchToAddProprety() throws IOException {
        App.setRoot("AddingPropretiesView");
    }

    /**
     * getPropertyList returns ObservableList<Property> from the properties
     * table inside the database.
     *
     * @return ObservableList<Property>
     */
    public ObservableList<Property> getPropertyList() {
        ObservableList<Property> propertyList = FXCollections.observableArrayList(); //create ObservableList<Contractors>
        DatabaseReferrer dbRef = DatabaseReferrer.getInstance();
        Connection conn = dbRef.returnConn();
        //create a connection from the returned connection object of the returnConn method
        String query = "SELECT * FROM properties"; //select all data from contractors table
        Statement statement;
        ResultSet resultset;

        try {
            statement = conn.createStatement();//create a statement from the connection object
            resultset = statement.executeQuery(query); //execute the query
            Property properties;
            while (resultset.next()) {
                //set properties list from information found in the properties table from the database
                properties = new Property(resultset.getInt("properties_id"),
                        resultset.getString("properties_type"), resultset.getString("properties_address"),
                        resultset.getInt("properties_totalunits"), resultset.getInt("properties_emptyunits"),
                        resultset.getString("properties_citytax"), resultset.getString("properties_schooltax"),
                        resultset.getString("properties_insurance"),
                        resultset.getInt("fk_properties_bankid"), resultset.getString("properties_mortgagesum"),
                        resultset.getInt("properties_loanlength"));
                propertyList.add(properties);

            }
        } catch (Exception exception) {
            exception.printStackTrace();//catch and print exceptions if any
        }
        return propertyList;
    }

    public ObservableList<Condo> getCondoList() {
        ObservableList<Condo> condoFeeList = FXCollections.observableArrayList();
        DatabaseReferrer dbRef = DatabaseReferrer.getInstance();
        Connection conn = dbRef.returnConn();
        //create a connection from the returned connection object of the returnConn method
        String query = "SELECT * FROM properties"; //select all data from contractors table
        Statement statement;
        ResultSet resultset;

        try {
            statement = conn.createStatement();//create a statement from the connection object
            resultset = statement.executeQuery(query); //execute the query
            Condo condos;
            while (resultset.next()) {
                //set contractors list from information found in the contractors table from the database
                condos = new Condo(resultset.getInt("properties_id"),
                        resultset.getString("properties_type"), resultset.getString("properties_address"),
                        resultset.getInt("properties_totalunits"), resultset.getInt("properties_emptyunits"),
                        resultset.getString("properties_citytax"), resultset.getString("properties_schooltax"),
                        resultset.getString("properties_insurance"),
                        resultset.getInt("fk_properties_bankid"), resultset.getString("properties_mortgagesum"),
                        resultset.getInt("properties_loanlength"), resultset.getString("properties_condofee"));
                condoFeeList.add(condos);

            }
        } catch (Exception exception) {
            exception.printStackTrace();//catch and print exceptions if any
        }
        return condoFeeList;
    }

    /**
     * showProperties sets the columns of the TableView using the
     * ObservableList<Property> returned by the getPropertyList() method.
     */
    public void showProperties() {
        ObservableList<Property> list = getPropertyList();
        ObservableList<Condo> condoFeeList = getCondoList();
        colId.setCellValueFactory(new PropertyValueFactory<Property, Integer>("propertyId")); //set name column
        colType.setCellValueFactory(new PropertyValueFactory<Property, String>("propertyType")); // set compant name column
        colAddress.setCellValueFactory(new PropertyValueFactory<Property, String>("propertyAddr")); //set phone number column
        colTotalUnits.setCellValueFactory(new PropertyValueFactory<Property, Integer>("totalUnits")); //set type of maintenace column
        colVacant.setCellValueFactory(new PropertyValueFactory<Property, Integer>("emptyUnits")); //set MaintenaceFee column
        colCityTax.setCellValueFactory(new PropertyValueFactory<Property, String>("cityTax"));
        colschoolTax.setCellValueFactory(new PropertyValueFactory<Property, String>("schoolTax")); //set Date column
        colInsurance.setCellValueFactory(new PropertyValueFactory<Property, String>("insurance"));
        colLoanAmount.setCellValueFactory(new PropertyValueFactory<Property, String>("mortgageAmount"));
        colLoanTime.setCellValueFactory(new PropertyValueFactory<Property, Integer>("loanLength"));
        colBankId.setCellValueFactory(new PropertyValueFactory<Property, Integer>("bankId"));
        colCondoFee.setCellValueFactory(new PropertyValueFactory<Condo, String>("condoFee"));
        PropertyView.setItems(list); //set TableView
        condoFeecol.setItems(condoFeeList);

    }

    @FXML
    private void handleMouseAction(MouseEvent event) {
        Property selectedProperty = PropertyView.getSelectionModel().getSelectedItem();
        tfPropId.setText("" + selectedProperty.getPropertyId());
        int bankId = selectedProperty.getBankId();
        int mortgageAmount = Integer.parseInt(selectedProperty.getMortgageAmount());
        int loanLength = selectedProperty.getLoanLength();
        double mortgageRate = DataManipulation.findMortgageRate(bankId);
        double montlyMortgage = Property.calculateMontlyMortgage(mortgageRate, mortgageAmount, loanLength);
        tfMontlyMortgage.setText("" + montlyMortgage);
    }

    /**
     * deleteBtn deletes record from properties table using the Properties
     * object selected in the TableView.
     *
     * @param toDelete Tenant
     */
    @FXML
    private void deleteBtn() {
        Property propertyToDelete = PropertyView.getSelectionModel().getSelectedItem();
        String query = "DELETE FROM properties WHERE properties_id=" + propertyToDelete.getPropertyId();
        DataManipulation.executeQuery(query);
        showProperties(); //display TableView data
    }

}
