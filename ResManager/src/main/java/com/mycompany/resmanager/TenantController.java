/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.resmanager;

import com.models.Tenant;
import com.mysqlconnection.DataManipulation;
import com.mysqlconnection.DatabaseReferrer;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class for TenantView
 *
 * @author Maria Barba
 * @version 1.0
 * @since 2021-04-27
 */
public class TenantController extends MainController implements Initializable {

    @FXML
    private TableView<Tenant> TenantTableview;
    @FXML
    private TableColumn<Tenant, String> col_sin;
    @FXML
    private TableColumn<Tenant, Integer> col_houseid;
    @FXML
    private TableColumn<Tenant, String> col_fullname;
    @FXML
    private TableColumn<Tenant, String> col_phone;
    @FXML
    private TableColumn<Tenant, String> col_email;
    @FXML
    private TableColumn<Tenant, String> col_rent;
    @FXML
    private TableColumn<Tenant, String> col_address;
    @FXML
    private TableColumn<Tenant, String> col_rentpaid;
    @FXML
    private TableColumn<Tenant, String> col_utilities;
    @FXML
    private TableColumn<Tenant, String> col_leaserenewd;
    @FXML
    private TableColumn<Tenant, String> col_rentoverdue;
    @FXML
    private TableColumn<Tenant, String> col_flag;
    @FXML
    private Button btn_add;
    @FXML
    private Button btn_delete;
    @FXML
    private Button btn_update;
    @FXML
    private TextField tfupdate_rent;
    @FXML
    private TextField tf_sin;

    /**
     * switchPropretyView switches view to PropretyView when Properties button
     * is clicked.
     *
     * @throws IOException
     */
    @FXML
    private void switchPropretyView() throws IOException {
        App.setRoot("PropretyView");
    }

    /**
     * switchToContractors switches view to ContractorsView when contractors
     * button is clicked.
     *
     * @throws IOException
     */
    @FXML
    private void switchToContractors() throws IOException {
        App.setRoot("ContractorsView");
    }

    /**
     * switchToInstitutionsswitches view to BankView when Institutions button is
     * clicked.
     *
     * @throws IOException
     */
    @FXML
    private void switchToInstitutions() throws IOException {
        App.setRoot("BankView");
    }

    /**
     * switchToLeases switches view to LeaseView when Leases button is clicked.
     *
     * @throws IOException
     */
    @FXML
    private void switchToLeases() throws IOException {
        App.setRoot("LeaseView");
    }

    /**
     * switchToAddTenant changes view to AddTenantView when Add button is
     * clicked.
     *
     * @throws IOException
     */
    @FXML
    private void switchToAddTenant() throws IOException {
        App.setRoot("AddTenant");
    }

    /**
     * getTenantsList returns ObservableList<Tenant> from the contractors table
     * inside the database.
     *
     * @return ObservableList<Tenant>
     */
    public ObservableList<Tenant> getTenantsList() {
        ObservableList<Tenant> tenantsList = FXCollections.observableArrayList(); //create ObservableList<Tenant>
        DatabaseReferrer dbRef = DatabaseReferrer.getInstance();
        Connection conn = dbRef.returnConn();
        //create a connection from the returned connection object of the returnConn method
        String query = "SELECT * FROM tenants"; //select all data from contractors table
        Statement statement;
        ResultSet resultset;

        try {
            statement = conn.createStatement();//create a statement from the connection object
            resultset = statement.executeQuery(query); //execute the query
            Tenant tenants;
            while (resultset.next()) {
                //set tenants list from information found in the tenants table from the database
                tenants = new Tenant(resultset.getString("tenants_sin"), resultset.getInt("tenants_houseid"),
                        resultset.getString("tenants_fullname"), resultset.getString("tenants_phone"),
                        resultset.getString("tenants_email"), resultset.getString("tenants_rent"),
                        resultset.getString("tenants_utilities"), resultset.getString("tenants_address"),
                        resultset.getString("tenants_rentpaid"), resultset.getString("tenants_leaserenewal"),
                        resultset.getString("tenants_overdue"), resultset.getString("tenants_flag"));
                tenantsList.add(tenants);

            }
        } catch (Exception exception) {
            exception.printStackTrace();//catch and print exceptions if any
        }
        return tenantsList;
    }

    /**
     * showTenants sets the columns of the TableView using the
     * ObservableList<Tenant> returned by the getTenantsList() method.
     */
    public void showTenants() {
        ObservableList<Tenant> list = getTenantsList();
        col_sin.setCellValueFactory(new PropertyValueFactory<Tenant, String>("sin"));
        col_houseid.setCellValueFactory(new PropertyValueFactory<Tenant, Integer>("houseid"));
        col_fullname.setCellValueFactory(new PropertyValueFactory<Tenant, String>("tenantName"));
        col_phone.setCellValueFactory(new PropertyValueFactory<Tenant, String>("phoneNumber"));
        col_email.setCellValueFactory(new PropertyValueFactory<Tenant, String>("email"));
        col_rent.setCellValueFactory(new PropertyValueFactory<Tenant, String>("monthlyRent"));
        col_address.setCellValueFactory(new PropertyValueFactory<Tenant, String>("address"));
        col_rentpaid.setCellValueFactory(new PropertyValueFactory<Tenant, String>("paidRent"));
        col_utilities.setCellValueFactory(new PropertyValueFactory<Tenant, String>("utilitiesCost"));
        col_leaserenewd.setCellValueFactory(new PropertyValueFactory<Tenant, String>("leaseRenewed"));
        col_rentoverdue.setCellValueFactory(new PropertyValueFactory<Tenant, String>("rentOverdue"));
        col_flag.setCellValueFactory(new PropertyValueFactory<Tenant, String>("flag"));
        TenantTableview.setItems(list); //set TableView

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        showTenants();
    }

    /**
     * deleteBtn deletes record from tenants table using the Contractors object
     * selected in the TableView.
     *
     * @param toDelete Tenant
     */
    @FXML
    private void deleteBtn() {
        Tenant tenantToDelete = TenantTableview.getSelectionModel().getSelectedItem();
        String query = "DELETE FROM tenants WHERE tenants_sin=" + "'" + tenantToDelete.getSin() + "'";
        DataManipulation.executeQuery(query);
        showTenants(); //display TableView data
    }
    /**
     * updateRent updates the monthly rent of a tenant using his sin to track
     * the information in the database
     */
    @FXML
    private void updateRent() {
        String query = "UPDATE tenants SET tenants_rent = '" + tfupdate_rent.getText() + "'"
                + "WHERE tenants_sin = '" + tf_sin.getText() + "'";
        DataManipulation.executeQuery(query);
        showTenants();//shows data in table once updated
    }
    /**
     * handleMouseAction waits that a row in table view is selected and
     * displays the monthly rent and tenant sin in the appropriate text fields
     * @param event MouseEvent
     */
    @FXML
    private void handleMouseAction(MouseEvent event) {
        Tenant selectedTenant = TenantTableview.getSelectionModel().getSelectedItem();
        tfupdate_rent.setText("" + selectedTenant.getMonthlyRent());
        tf_sin.setText("" + selectedTenant.getSin());

    }

}
