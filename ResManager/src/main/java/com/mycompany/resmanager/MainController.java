/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.resmanager;

import java.util.regex.Pattern;
import javafx.scene.control.Alert;

/**
 * FXML Controller class for MainController.
 * Class that will create methods across all controller classes.
 * @author Maria Barba
 * @version 1.0
 * @since 2021-05-13
 */
public class MainController {
   
    /**
     * createErrorBox creates an Alert box with the provided message.
     * @param message 
     */
    public void createErrorBox(String message){
         Alert alert = new Alert(Alert.AlertType.ERROR); 
         alert.setContentText(message);
         alert.showAndWait();
    }
     /**
     * createWarningBox creates a warning Alert box with the provided message.
     * @param message 
     */
    public void createWarningBox(String message){
         Alert alert = new Alert(Alert.AlertType.WARNING); 
         alert.setContentText(message);
         alert.showAndWait();
    }
    /**
     * isNumber checks if a field is numeric when the user must input a number to
     * be saved to the database
     * @param str String
     */
    public boolean isNumber(String str) { 
    boolean result=true;
    try {  
    Double.parseDouble(str);  
    
    } catch(NumberFormatException e){  
    result=false;
    createErrorBox(str + " must be numeric !"); 
    } 
    return result;
    }   
    /**
     * isPhoneNumber checks that the input String for phone number is numeric and 10 charachters long
     * @param phoneNumber String
     */
    public boolean isPhoneNumber(String phoneNumber){
        boolean result = true;
        try{
             Integer.parseInt(phoneNumber);  
             //check if the phone number is numeric
             if(phoneNumber.length()!=10){
                 result = false;
                 throw new NumberFormatException();
                 //check that the phone number lenth is 10
             }
        }catch(NumberFormatException e){  
        createErrorBox(phoneNumber + " must be a phone number!"); 
        //else create an error box saying that the phone has to be numeric
        }  
        return result;
        }
        /**
         * isSin checks that the input social insurance number is numeric and is 9 chars long
         * @param sin String
         */
        public boolean isSin(String sin){
        boolean result = true;
        try{
             Integer.parseInt(sin);  
             //check if the sin is numeric
             if(sin.length()!=9){
                 result = false;
                 throw new NumberFormatException();
                 //check that the sin is 9
             }
        }catch(NumberFormatException e){  
        createErrorBox(sin + " must be a phone number!"); 
        //else create an error box saying that the sin has to be numeric and 9 digits long
        }  
        return result;
    }
    /**
     * isEmail checks that the input string matches the email address , if it is 
     * false , then throw an error that  id output into an AlertBox
     * @param email String2
     */
    public boolean isEmail(String email){
        boolean result = true;
        //create a String to store the regex for an email
         String regexForEmail = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                            "[a-zA-Z0-9_+&*-]+)*@" +
                            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                            "A-Z]{2,7}$";
        //compare the pattern for email
        Pattern pattern = Pattern.compile(regexForEmail);
        try{
        if(!(pattern.matcher(email).matches())){
         //if the input email doesn't match the email pattern throw an exception
        result=false;
        throw new Exception(email + " doesn't apply to a valid email pattern !");
      
        }
        }catch(Exception e){
                //when the exception is caught, get the text from the exception and
                //put it into an Alert box
               createErrorBox(e.getMessage()); 
        }
        return result;
    }
    
}
