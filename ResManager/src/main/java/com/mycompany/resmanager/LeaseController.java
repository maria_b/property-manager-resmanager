/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.resmanager;

import com.models.Lease;
import com.mysqlconnection.DataManipulation;
import com.mysqlconnection.DatabaseReferrer;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * FXML Controller class for LeaseView
 *
 * @author Maria Barba
 * @version 1.0
 * @since 2021-04-19
 * 
 */
public class LeaseController extends MainController implements Initializable {

    @FXML
    private Button btnFileChoose;
    @FXML
    private TableView<Lease> leaseInfoTable;
    @FXML
    private TableColumn<Lease, String> colSin;
    @FXML
    private TableColumn<Lease, String> colPath;
    @FXML
    private Text errorText;
    @FXML
    private ComboBox<String> comboBox;
    @FXML
    private Button btnDelete;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<String> sinList = DataManipulation.getSinList();
        comboBox.setItems(sinList);
        showLease();

    }
   
    /**
     * openFileChooser will create a FileChooser object that will select 
     * a File and then will save the file's absolute path to the leases table in the 
     * database.
     * @param event ActionEvent
     */
    @FXML
    private void openFileChooser(ActionEvent event) {
        FileChooser chooseFile = new FileChooser(); //will create a FileChooser object
        chooseFile.getExtensionFilters().addAll(new ExtensionFilter("PDF", "*.pdf"));
        //will only allow pdf files to be selected 
        File chosenFile = chooseFile.showOpenDialog(null);
        //get the chosen file
        
        //if a file is chosen, proceed to store the path
        if (chosenFile != null) {
            //get the absolute path
            String leasePath = chosenFile.getAbsolutePath();
            System.out.println(leasePath);
            leasePath=leasePath.replace("\\", "/");
            //replace the java escape charachter by forward slash
            //get the sin 
            String sin = comboBox.getValue();
            //add to database
            addLeaseInfo(sin, leasePath);
           

        } else {
            //if no file was chosen make an alert box pop up
            super.createErrorBox("You need to choose a pdf file !");
        }
    }

    /**
     * getLeaseList returns an ObservableList of the Lease object type from the
     * leases table to be displayed in the TableView
     *
     * @return ObservableList<Lease> using data from the leases table inside the
     * database
     */
    public ObservableList<Lease> getLeaseList() {
        //creates an ObservableList<Lease>
        ObservableList<Lease> leaseList = FXCollections.observableArrayList();
        DatabaseReferrer dbRef = DatabaseReferrer.getInstance();
        Connection conn = dbRef.returnConn(); //calls method to return Connection object
        String query = "SELECT * FROM leases"; // select all data from banks table
        Statement statement;
        ResultSet resultset;

        try {
            statement = conn.createStatement(); //create a statement
            resultset = statement.executeQuery(query); //create a resultset from the statement
            Lease leases;
            while (resultset.next()) {
                leases = new Lease(resultset.getString("leases_sin"), resultset.getString("leases_path"));
                leaseList.add(leases); //add Lease object to observableList
            }
        } catch (Exception exception) {
            //if an error is caught, write it in the error text
            errorText.setText("An error occured");
        }
        return leaseList;
    }

    /**
     * showLease sets the TableView and the columns using the observable list
     * returned from the getBankList method
     */
    public void showLease() {
        ObservableList<Lease> list = getLeaseList(); //get the Lease list
        colSin.setCellValueFactory(new PropertyValueFactory<Lease, String>("SIN")); //set banks id column
        colPath.setCellValueFactory(new PropertyValueFactory<Lease, String>("path")); //set bank name column
        leaseInfoTable.setItems(list); //set the leases tbale with the appropriate information
    }
    /**
     * addLeaseInfo adds to the database the sin selected by the user,
     * and the path of the pdf file selected by the user.
     * @param sin String
     * @param leasePath String
     */
    public void addLeaseInfo(String sin, String leasePath) {
        checkFormFileds(); // before adding make sure all fields are filled
        if(DataManipulation.checkIfLeaseExists(sin)){
            //if a Lease for the selected sin exists output an error
            errorText.setText("An error occured !");
        }else{
            
        // check if Lease already exists , if it does
        //create an Alert box
        String query = "INSERT INTO leases VALUES ('" + sin + "','"
                + leasePath + "')";
        //insert values into leases
        DataManipulation.executeQuery(query);
        showLease(); //display data in the TableView
        }
    }
     /**
     * deleteBtn deletes the Lease and sin from the table 
     * @param toDelete Lease
     */
    @FXML
    private void deleteBtn() {
        
        String query = "DELETE FROM leases WHERE leases_sin=" + "'" + comboBox.getValue() + "'";
        DataManipulation.executeQuery(query);
        showLease(); //display TableView data
    }
    

    /**
     * switchPropretyView switches view to PropretyView when Properties button
     * is clicked.
     *
     * @throws IOException
     */
    @FXML
    private void switchPropretyView() throws IOException {
        App.setRoot("PropretyView");
    }

    /**
     * switchToContractors changes view to ContractorsView when the Contractors
     * button is clicked.
     *
     * @throws IOException
     */
    @FXML
    private void switchToContractors() throws IOException {
        App.setRoot("ContractorsView");
    }

    /**
     * switchToTenants switches view to TenantView when the Tenants button is
     * clicked
     *
     * @throws IOException
     */
    @FXML
    private void switchToTenants() throws IOException {
        App.setRoot("TenantView");
    }

    /**
     * switchToInstitutions switches view to BankView when Institutions button
     * is clicked
     *
     * @throws IOException
     */
    @FXML
    private void switchToInstitutions() throws IOException {
        App.setRoot("BankView");
    }
    /**
     * checkFormFileds checks that the user input a tenant sin for a Lease file
     * to output an error
     */
    public void checkFormFileds() {
        if (comboBox.getSelectionModel().isEmpty()) {
            super.createErrorBox("Fill all fields before saving to the database !");

        }
    }
   
}
