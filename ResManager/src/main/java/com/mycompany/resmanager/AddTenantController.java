/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.resmanager;

import com.mysqlconnection.DataManipulation;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class for AddTenant
 *
 * @author Maria Barba
 * @version 1.0
 * @since 2021-04-27
 */
public class AddTenantController extends MainController implements Initializable {

    @FXML
    private TextField tf_sin;
    @FXML
    private TextField tf_houseid;
    @FXML
    private TextField tf_fullname;
    @FXML
    private TextField tf_phone;
    @FXML
    private TextField tf_email;
    @FXML
    private TextField tf_address;
    @FXML
    private TextField tf_rent;
    @FXML
    private TextField tf_cost;
    @FXML
    private TextField tf_flag;
    @FXML
    private Button btn_flag;
    @FXML
    private Button btn_save;
    @FXML
    private CheckBox ch_rentpaid;
    @FXML
    private CheckBox ch_lease;
    @FXML
    private CheckBox ch_rentoverdue;

    private String rentPaid;
    private String leaseRenewed;
    private String rentOverdue;
    //creates AddTenantController object to keep track of rentPaid,leaseRenewed,rentOverdue
    private static AddTenantController tenantInfo = new AddTenantController();
    @FXML
    private Button btn_unflag;
    @FXML
    private Button btn_returnTenant;

    /**
     * Getter for rent paid
     *
     * @return String rentPaid
     */
    public String getRentPaid() {
        return rentPaid;
    }

    /**
     * Setter for rent paid
     *
     * @param rentPaid String
     */
    public void setRentPaid(String rentPaid) {
        this.rentPaid = rentPaid;
    }

    /**
     * Getter for Lease Renewed
     *
     * @return String
     */
    public String getLeaseRenewed() {
        return leaseRenewed;
    }

    /**
     * Setter for leaseRenewed
     *
     * @param leaseRenewed String
     */
    public void setLeaseRenewed(String leaseRenewed) {
        this.leaseRenewed = leaseRenewed;
    }

    /**
     * Getter for rent overdue
     *
     * @return String rentOverdue
     */
    public String getRentOverdue() {
        return rentOverdue;
    }

    /**
     * Setter for rentOverdue
     *
     * @param rentOverdue String
     */
    public void setRentOverdue(String rentOverdue) {
        this.rentOverdue = rentOverdue;
    }

    /**
     * Setter for rent status id checkbox is selected
     *
     * @param event
     */
    @FXML
    private void setRentStatus(ActionEvent event) {
        if (ch_rentpaid.isSelected()) {
            tenantInfo.setRentPaid("T");
        }
    }

    /**
     * Setter for Lease Renewed if checkbox is checked
     *
     * @param event
     */
    @FXML
    private void setLeaseRenewedStatus(ActionEvent event) {
        if (ch_lease.isSelected()) {
            tenantInfo.setLeaseRenewed("T");
        }
    }

    /**
     * Setter for rent overdue if checkbox is checked
     *
     * @param event ActionEvent
     */
    @FXML
    private void setRentOverdueStatus(ActionEvent event) {
        if (ch_rentoverdue.isSelected()) {
            tenantInfo.setRentOverdue("T");
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // by default sets value false for all checkboxes
        tenantInfo.setRentPaid("F");
        tenantInfo.setLeaseRenewed("F");
        tenantInfo.setRentOverdue("F");

    }

    /**
     * addTenantsData inserts data into the tenants table using information from
     * the text fields and date filed.
     */
    @FXML
    private void addTenantsData() {
        if(!( checkFormFileds())){
        //if checkFormFileds returns false, meaning no fields are empty and data types are respected, add 
        //new data
        String query = "INSERT INTO tenants VALUES ('" + tf_sin.getText() + "',"
                + tf_houseid.getText() + ",'" + tf_fullname.getText() + "','"
                + tf_phone.getText() + "','" + tf_email.getText() + "','"
                + tf_rent.getText() + "','" + tf_cost.getText() + "','"
                + tf_address.getText() + "','" + tenantInfo.getRentPaid() + "','"
                + tenantInfo.getLeaseRenewed() + "','" + tenantInfo.getRentOverdue() + "','"
                + tf_flag.getText() + "')";
        DataManipulation.executeQuery(query);
        super.createWarningBox("Tenant " + tf_fullname.getText() + " has been added.");
        }
    }

    /**
     * switchToTenants switches view to TenantView
     *
     * @throws IOException
     */
    @FXML
    private void switchToTenants() throws IOException {
        App.setRoot("TenantView");

    }

    /**
     * handleButtonAction checks for the buttons that were clicked and changes
     * the flag status of a tenant
     *
     * @param event
     */
    @FXML
    private void handleButtonAction(ActionEvent event) {
        Alert flagTenant = new Alert(AlertType.NONE);//create an alert box
        flagTenant.setAlertType(AlertType.WARNING);
        if (event.getSource() == btn_flag) {
            //when flag button is clicked
            tf_flag.setText("True");
            //set value true for flag
            flagTenant.setContentText(" Attention ! Rent very overdue for this tenant !!! ");
            //display the message to the user
            flagTenant.show();
        }
        if (event.getSource() == btn_unflag) {
            //when button unflag is clicked,
            tf_flag.setText("False");
            //set the value false for flag
            flagTenant.setContentText(" Attention ! Tenant is no longer flagged ! ");
            //display the message to the user
            flagTenant.show();
        }
    }

    /**
     * checkPropertyExistence checks if the property with the input id exists
     * and calls the checkIfPropertyExists from DataManipulation class
     *
     * @param event
     */
    @FXML
    private void checkPropertyExistence(MouseEvent event) {
        String addrForId = DataManipulation.checkIfPropertyExists(Integer.parseInt(tf_houseid.getText()));
        //finds the address for the input property id
        tf_address.setText(addrForId);
        //sets the value of the address in the address field
    }

    /**
     * checkFormFileds checks if some fields in the form are missing values and
     * promts the user to enter all data and if the data types are respected, if not then returns
     * true
     */
    public boolean checkFormFileds() {
        boolean result=false;
        //check that no field is empty
        if (tf_sin.getText().isEmpty() | tf_houseid.getText().isEmpty()
                | tf_fullname.getText().isEmpty() | tf_phone.getText().isEmpty()
                | tf_email.getText().isEmpty() | tf_address.getText().isEmpty()
                | tf_rent.getText().isEmpty() | tf_cost.getText().isEmpty()
                | tf_flag.getText().isEmpty()) {
            result=true;
            super.createErrorBox("Fill all fields before saving to the database !");
        }
        //check that all fields respect their type
        if(!(super.isEmail(tf_email.getText())) || !(super.isNumber(tf_phone.getText()))
                || !(super.isSin(tf_sin.getText())) || !(super.isNumber(tf_houseid.getText()))
                || !(super.isNumber(tf_rent.getText())) || !(super.isNumber(tf_cost.getText()))){
            result=true;
            //if error in data type , set to true 
        }   
        return result;
    }

}
