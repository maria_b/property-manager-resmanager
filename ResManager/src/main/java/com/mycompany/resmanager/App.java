package com.mycompany.resmanager;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import com.mysqlconnection.*;
import java.io.IOException;
import javafx.scene.image.Image;

/**
 * JavaFX App
 * @author Maria Barba
 * @version 1.0
 * @since 2021-04-10
 */
        
public class App extends Application {

    private static Scene scene;
    
    /**
     * The method start loads the stage for views
     * @param stage
     * @throws IOException 
     */
    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("StartView"), 900, 500);
        stage.setTitle("ResManager - Maria Barba 1932657"); 
        stage.setScene(scene);
        stage.sizeToScene();
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));

    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }
    /**
     * Main method launches the program and connects to the database
     * @param args 
     */
    public static void main(String[] args) {
        DatabaseReferrer dbRef= DatabaseReferrer.getInstance();
        dbRef.returnConn();
        DataManipulation.createTables();
        
        launch();

    }

}
