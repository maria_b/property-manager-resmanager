/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.resmanager;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import java.sql.*;
import com.models.Contractors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import com.mysqlconnection.*;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class for ContractorsView
 * @author Maria Barba
 * @version 1.0
 * @since 2021-04-19
 */
public class ContractorsController extends MainController implements Initializable {

    @FXML
    private TableView<Contractors> tableContractors;
    @FXML
    private TableColumn<Contractors, String> colName;
    @FXML
    private TableColumn<Contractors, String> colPhone;
    @FXML
    private TableColumn<Contractors, String> colCompany;
    @FXML
    private TableColumn<Contractors, String> colType;
    @FXML
    private TableColumn<Contractors, String> colPrice;
    @FXML
    private TableColumn<Contractors, Date> colDate;
    @FXML
    private TextField tfName;
    @FXML
    private TextField tfPhone;
    @FXML
    private TextField tfCompany;
    @FXML
    private TextField tfMaintenace;
    @FXML
    private TextField tfCost;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnDelete;
    @FXML
    private DatePicker tfDate;
    @FXML
    private ComboBox combo_box;

    /**
     * Initializes the controller class.
     * Creates an  ObservableList<String>  for type of maintenance and sets the values to the 
     * combo box. Calls the showContractors method to display data in TableView.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ObservableList<String> typeMaintenanceList = FXCollections.observableArrayList("Electricity", "Plumbing", "Painting", "Washing", "Inspection", "Exterminator", "Landscaping", "Renovations");
        combo_box.setItems(typeMaintenanceList);
        showContractors();

    }
    /**
     * switchPropretyView switches the view to PropretyView when the Properties button is clicked.
     * @throws IOException 
     */
    @FXML
    private void switchPropretyView() throws IOException {
        App.setRoot("PropretyView");
    }
    /**
     * switchToInstitutions switches view to BankView when the institutions button is clicked.
     * @throws IOException 
     */
    @FXML
    private void switchToInstitutions() throws IOException {
        App.setRoot("BankView");
    }
    /**
     * switchToTenants changes view to TenantView when the Tenants button is clicked.
     * @throws IOException 
     */
    @FXML
    private void switchToTenants() throws IOException {
        App.setRoot("TenantView");
    }
    /**
     * switchToLeases changes view to LeaseView when the Leases button is clicked.
     * @throws IOException 
     */
    @FXML
    private void switchToLeases() throws IOException {
        App.setRoot("LeaseView");
    }
    /**
     * getContractorsList returns ObservableList<Contractors>  from the contractors table
     * inside the database.
     * @return ObservableList<Contractors>
     */
    public ObservableList<Contractors> getContractorsList() {
        ObservableList<Contractors> contractorsList = FXCollections.observableArrayList(); //create ObservableList<Contractors>
        DatabaseReferrer dbRef= DatabaseReferrer.getInstance();
        Connection conn = dbRef.returnConn();
       //create a connection from the returned connection object of the returnConn method
        String query = "SELECT * FROM contractors"; //select all data from contractors table
        Statement statement;
        ResultSet resultset;

        try {
            statement = conn.createStatement();//create a statement from the connection object
            resultset = statement.executeQuery(query); //execute the query
            Contractors contractors; 
            while (resultset.next()) {
                //set contractors list from information found in the contractors table from the database
                contractors = new Contractors(resultset.getString("contractors_name"), resultset.getString("contractors_company"), resultset.getString("contractors_phone"), resultset.getString("contractors_type"), resultset.getString("contractors_price"), resultset.getDate("contractors_date"));
                contractorsList.add(contractors); 

            }
        } catch (Exception exception) {
            exception.printStackTrace();//catch and print exceptions if any
        }
        return contractorsList;
    }
    
    /**
     * showContractors sets the columns of the TableView using the ObservableList<Contractors> returned by the
     * getContractorsList() method.
     */
    public void showContractors() {
        ObservableList<Contractors> list = getContractorsList();
        colName.setCellValueFactory(new PropertyValueFactory<Contractors, String>("contractorName")); //set name column
        colCompany.setCellValueFactory(new PropertyValueFactory<Contractors, String>("companyName")); // set compant name column
        colPhone.setCellValueFactory(new PropertyValueFactory<Contractors, String>("phoneNumber")); //set phone number column
        colType.setCellValueFactory(new PropertyValueFactory<Contractors, String>("typeOfMaintenace")); //set type of maintenace column
        colPrice.setCellValueFactory(new PropertyValueFactory<Contractors, String>("maintenanceFee")); //set MaintenaceFee column
        colDate.setCellValueFactory(new PropertyValueFactory<Contractors, Date>("date")); //set Date column
        tableContractors.setItems(list); //set TableView

    }
    
    /**
     * addContractorsData inserts data into the contractors table using information from the text fields and date filed.
     */
    @FXML
    private void addContractorsData() {
        if (!(checkFormFileds())){
        //if there are no data type errors and if all fields are filled, execute query
        String query = "INSERT INTO contractors VALUES ('" + tfName.getText() + "','" + tfPhone.getText() + "','" + tfCompany.getText() + "','" + tfMaintenace.getText() + "','" + tfCost.getText() + "','" + tfDate.getValue() + "')";
        DataManipulation.executeQuery(query);
        }
        showContractors(); //display data in the TableView
    }
  
    
    /**
     * deleteBtn deletes record from contractors table using the Contractors object selected in the TableView.
     * @param toDelete Contractors
     */
    @FXML
    private void deleteBtn(){
        Contractors contractorToDelete = tableContractors.getSelectionModel().getSelectedItem();
        String query = "DELETE FROM contractors WHERE contractors_name='" + contractorToDelete.getContractorName()+ "'";
        DataManipulation.executeQuery(query);
        showContractors(); //display TableView data
    }
    
    /**
     * setComboBoxChoice gets the selected choice of type of maintenance in the combo box and sets 
    the text field of type of maintenance accordingly.
     * @param event ActionEvent
     */
    @FXML
    private void setComboBoxChoice(ActionEvent event) {
        String selectedChoice = combo_box.getSelectionModel().getSelectedItem().toString();//get the selected choice
        tfMaintenace.setText(selectedChoice); //set text 
    }
    /**
     * Checks if there are any empty fields in the form and promts the user to enter data 
     * and checks if the input phone number and cost are numeric and actually the phone number is
     * a real phone number.
     * @return result boolean , true if empty field or wrong data type.
     */
    public boolean  checkFormFileds(){
        boolean result=false;
            if(tfName.getText().isEmpty() |  tfPhone.getText().isEmpty() | 
                   tfCompany.getText().isEmpty() | tfMaintenace.getText().isEmpty() |
                    tfCost.getText().isEmpty()){
            result=true;
            super.createErrorBox("Fill all fields before saving to the database !");
             //create an error box if a field is empty
            }
            if(!(super.isPhoneNumber(tfPhone.getText())) || !(super.isNumber(tfCost.getText()))){
            result=true;    
            //create an error if wrong datatype
            }
            return result;
    }
    
}
