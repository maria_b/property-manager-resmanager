/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.resmanager;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import com.mysqlconnection.DataManipulation;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;

/**
 ** Controller class for AddingPropetiesView
 *
 * @author Maria Barba
 * @version 1.0
 * @since 2021-04-19
 */
public class AddingPropertiesController extends MainController implements Initializable {

    @FXML
    private TextField tfAddr;
    @FXML
    private RadioButton tfRBC;
    @FXML
    private RadioButton tfTD;
    @FXML
    private RadioButton tfBMO;
    @FXML
    private RadioButton tfCIBC;
    @FXML
    private TextField tfTotalUnits;
    @FXML
    private TextField tfVacantUnits;
    @FXML
    private TextField tfCityTax;
    @FXML
    private TextField tfSchoolTax;
    @FXML
    private TextField tfLoanAmount;
    @FXML
    private TextField tfInsurance;
    @FXML
    private TextField tfCondoFee;
    @FXML
    private TextField tfPropertyId;
    @FXML
    private ComboBox<String> comboBox;
    @FXML
    private TextField tfLoanLength;
    @FXML
    private TextField tfPropertyType;
    @FXML
    private ToggleGroup banks;

    private int bankId;
    //bankInfo object stores the bankId that is found for the selected bank name by the user
    private static AddingPropertiesController bankInfo = new AddingPropertiesController();
    @FXML
    private Button btnReturnProperty;
    @FXML
    private Button btnSave;

    /**
     * Getter for bankId
     *
     * @return bankId int
     */
    public int getBankId() {
        return bankId;
    }

    /**
     * Setter for bankId
     *
     * @param bankId int
     */
    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //set values for combo box
        ObservableList<String> typeProperty = FXCollections.observableArrayList("House", "Plex", "Condo");
        comboBox.setItems(typeProperty);
    }

    /**
     * switchPropretyView changes view to PropretyView (when Save button is
     * clicked)
     *
     * @throws IOException
     */
    @FXML
    private void switchPropretyView() throws IOException {
        App.setRoot("PropretyView");
    }

    /**
     * setComboBoxChoice sets the values from the combo box to the
     * tfPropertyType field
     *
     * @param event ActionEvent
     */
    @FXML
    private void setComboBoxChoice(ActionEvent event) {
        String selectedProperty = comboBox.getSelectionModel().getSelectedItem().toString();
        tfPropertyType.setText(selectedProperty);
        if ((selectedProperty).equals("House")) {
            //if the selected property is house
            setDefaultHouseVals();
            //set all the default values for a house
        } else if ((selectedProperty).equals("Plex")) {
            //if selected property is plex
            setDefaultPlexVals();
            //set all the default values for plex
        }
    }

    /**
     * setDefaultHouseVals sets all the default values for a House
     */
    public void setDefaultHouseVals() {
        tfCondoFee.setText("0");//house has no condo fee
        tfTotalUnits.setText("1");//house has only one value
    }

    /**
     * setDefaultPlexVals sets all the default values for a Plex
     */
    public void setDefaultPlexVals() {
        tfCondoFee.setText("0");//plex has no condo fee
    }

    /**
     * setBankNmae looks at what bank name is selected by the user and finds the
     * bankId of the selected bank name from the banks table .
     *
     * @param event ActionEvent
     */
    @FXML
    private void setBankNmae(ActionEvent event) {
        //get the selected radio button in the toggle group
        RadioButton selectedRb = (RadioButton) banks.getSelectedToggle();
        String toogleGroupVal = selectedRb.getText();
        //Find bank id for that selected bank name
        int bankIdForSelected = DataManipulation.getBankId(toogleGroupVal);
        bankInfo.setBankId(bankIdForSelected);
    }

    /**
     * addPropertiesData inserts data into the properties table using
     * information from the text fields and selected bank.
     */
    @FXML
    private void addPropertiesData() {

        if (!(checkFormFileds())) {

            //verify that all fields are filled and there is no input value error to add data
            String query = "INSERT INTO properties VALUES (" + tfPropertyId.getText() + ",'"
                    + tfPropertyType.getText() + "','" + tfAddr.getText() + "',"
                    + tfTotalUnits.getText() + "," + tfVacantUnits.getText() + ",'"
                    + tfCityTax.getText() + "','" + tfSchoolTax.getText() + "','"
                    + tfInsurance.getText() + "'," + bankInfo.getBankId() + ",'"
                    + tfLoanAmount.getText() + "','" + tfLoanLength.getText() + "','"
                    + tfCondoFee.getText() + "')";
            DataManipulation.executeQuery(query);
            super.createWarningBox("Property of" + tfPropertyId.getText() + " has been added.");
        }

    }

    /**
     * checkFormFileds checks if all values in the form are filled and promts
     * the user to fill all data.
     */
    public boolean checkFormFileds() {
        boolean result = false;
        if (tfAddr.getText().isEmpty() || tfTotalUnits.getText().isEmpty()
                || tfVacantUnits.getText().isEmpty() || tfCityTax.getText().isEmpty()
                || tfSchoolTax.getText().isEmpty() || tfLoanAmount.getText().isEmpty()
                || tfInsurance.getText().isEmpty() || tfCondoFee.getText().isEmpty()
                || tfPropertyId.getText().isEmpty() || tfLoanLength.getText().isEmpty()
                || tfPropertyType.getText().isEmpty()) {
            //if not all fields are filled, return true for error
            result = true;
            super.createErrorBox("Fill all fields before saving to the database !");
        }
        if (!(super.isNumber(tfTotalUnits.getText())) || !(super.isNumber(tfVacantUnits.getText()))
                || !(super.isNumber(tfCityTax.getText())) || !(super.isNumber(tfSchoolTax.getText()))
                || !(super.isNumber(tfLoanAmount.getText())) || !(super.isNumber(tfInsurance.getText()))
                || !(super.isNumber(tfCondoFee.getText())) || !(super.isNumber(tfPropertyId.getText()))
                || !(super.isNumber(tfLoanLength.getText()))) {
            //if the appropriate fields are not numeric, return true for error
            result = true;
        }
        return result;
    }

}
