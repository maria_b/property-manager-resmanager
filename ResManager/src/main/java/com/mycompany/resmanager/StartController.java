/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.resmanager;


import java.io.IOException;
import javafx.fxml.FXML;

/**
 * FXML Controller class for StartView
 * @author Maria Barba
 * @version 1.0
 * @since 2021-04-19
 */
public class StartController {
    /**
     * Switches view to PropretyView when Properties button is clicked.
     * @throws IOException 
     */
    @FXML
    private void switchPropretyView() throws IOException {
        App.setRoot("PropretyView");
    }
}
