/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mysqlconnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;

/**
 * Class that manipulates database
 *
 * @author Maria Barba
 * @version 1.0
 * @since 2021-04-19
 */
public class DataManipulation {

    /**
     * executeQuery takes as input a query String,creates a statement and
     * executes update on the contractors table.
     *
     * @param query String
     */
    public static void executeQuery(String query) {
        DatabaseReferrer dbRef = DatabaseReferrer.getInstance();
        Connection conn = dbRef.returnConn();
        //create a connection from the Connection object returned
        Statement statement;
        try {
            statement = conn.createStatement();
            statement.executeUpdate(query);

            conn.close(); //close the connection
        } catch (Exception exception) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("An error occured !");
            a.show();
            //exception.printStackTrace(); //catch and print exceptions if any found
        }
    }

    /**
     * dropTables drops tables from the database if they exist
     */
    public static void dropTables() {
        //delete leases table if exists
        String deleteLeases = "DROP TABLE IF EXISTS leases";
        executeQuery(deleteLeases);
        //delete contractors table if exists
        String deleteContractors = "DROP TABLE IF EXISTS  contractors";
        executeQuery(deleteContractors);
        //delete tenants table if exists
        String deleteTenants = "DROP TABLE IF EXISTS tenants";
        executeQuery(deleteTenants);
        //drop properties tables if exists
        String deleteProperties = "DROP TABLE IF EXISTS properties";
        executeQuery(deleteProperties);
        //drop banks table if exists
        String deleteBanks = "DROP TABLE IF EXISTS banks";
        executeQuery(deleteBanks);
        
    }

    /**
     * createTables creates tables if they don't already exist in the database
     * using the executeQuery method
     */
    public static void createTables() {
        //create Banks table
        String createBanks = "CREATE TABLE IF NOT EXISTS `banks` (\n"
                + "   `bank_id` int NOT NULL,\n"
                + "   `bank_name` varchar(10) DEFAULT NULL,\n"
                + "   `bank_mortgage` varchar(7) DEFAULT NULL,\n"
                + "    PRIMARY KEY(`bank_id`)\n"
                + " )";
        executeQuery(createBanks);
        //create Contractors table
        String createContractors = "CREATE TABLE IF NOT EXISTS `contractors` (\n"
                + "   `contractors_name` varchar(30) NOT NULL,\n"
                + "   `contractors_phone` varchar(10) NOT NULL,\n"
                + "   `contractors_company` varchar(30) NOT NULL,\n"
                + "   `contractors_type` varchar(15) NOT NULL,\n"
                + "   `contractors_price` varchar(9) DEFAULT NULL,\n"
                + "   `contractors_date` date DEFAULT NULL,\n"
                + "    PRIMARY KEY (`contractors_name`),\n"
                + "    UNIQUE KEY `contractors_phone_UNIQUE` (`contractors_phone`)\n"
                + " )";
        executeQuery(createContractors);
        //Create properties table
        String createProperties = "CREATE TABLE IF NOT EXISTS `properties` (\n"
                + "   `properties_id` int NOT NULL,\n"
                + "   `properties_type` varchar(10) NOT NULL,\n"
                + "   `properties_address` varchar(50) NOT NULL,\n"
                + "   `properties_totalunits` int DEFAULT NULL,\n"
                + "   `properties_emptyunits` int DEFAULT NULL,\n"
                + "   `properties_citytax` varchar(8) NOT NULL,\n"
                + "   `properties_schooltax` varchar(8) NOT NULL,\n"
                + "   `properties_insurance` varchar(8) NOT NULL,\n"
                + "   `fk_properties_bankid` int DEFAULT NULL,\n"
                + "   `properties_mortgagesum` varchar(12) DEFAULT NULL,\n"
                + "   `properties_loanlength` varchar(3) DEFAULT NULL,\n"
                + "   `properties_condofee` varchar(10) DEFAULT NULL,\n"
                + "   PRIMARY KEY (`properties_id`),\n"
                + "   FOREIGN KEY(`fk_properties_bankid`) REFERENCES banks(`bank_id`) ON DELETE CASCADE\n"
                + ")";
        executeQuery(createProperties);
        //Create tenants table
        String createTenants = "CREATE TABLE IF NOT EXISTS `tenants` (\n"
                + "   `tenants_sin` varchar(9) NOT NULL,\n"
                + "   `tenants_houseid` int NOT NULL,\n"
                + "   `tenants_fullname` varchar(30) NOT NULL,\n"
                + "   `tenants_phone` varchar(10) NOT NULL,\n"
                + "   `tenants_email` varchar(45) DEFAULT NULL,\n"
                + "   `tenants_rent` varchar(8) DEFAULT NULL,\n"
                + "   `tenants_utilities` varchar(8) DEFAULT NULL,\n"
                + "   `tenants_address` varchar(50) DEFAULT NULL,\n"
                + "   `tenants_rentpaid` varchar(8) DEFAULT NULL,\n"
                + "   `tenants_leaserenewal` varchar(8) DEFAULT NULL,\n"
                + "   `tenants_overdue` varchar(8) DEFAULT NULL,\n"
                + "   `tenants_flag` varchar(8) DEFAULT NULL,\n"
                + "   PRIMARY KEY (`tenants_sin`),\n"
                + "   UNIQUE KEY `tenants_sin_UNIQUE` (`tenants_sin`),\n"
                + "   UNIQUE KEY `tenants_phone_UNIQUE` (`tenants_phone`),\n"
                + "   FOREIGN KEY(`tenants_houseid`) REFERENCES properties(`properties_id`) ON DELETE CASCADE\n"
                + " )";
        executeQuery(createTenants);
        //Create leases table
        String createLeases = "CREATE TABLE IF NOT EXISTS `leases` (\n"
                + "   `leases_sin` varchar(9) NOT NULL,\n"
                + "   `leases_path` varchar(100) NOT NULL,\n"
                + "   FOREIGN KEY(`leases_sin`) REFERENCES tenants(`tenants_sin`) ON DELETE CASCADE\n"
                + " )";
        executeQuery(createLeases);
    }

    /**
     * findMortgageRate takes a bankId and finds the mortgage rate for that bank
     *
     * @param bankId
     * @return double mortgage rate
     */
    public static double findMortgageRate(int bankId) {
        DatabaseReferrer dbRef = DatabaseReferrer.getInstance();
        Connection conn = dbRef.returnConn();
        //create a connection from the returned connection object of the returnConn method
        String query = "SELECT bank_mortgage FROM banks WHERE bank_id=" + bankId + "";
        Statement statement;
        ResultSet resultset;
        String mortgageRate;
        double mortgageDouble = 0.0;//set default to 0 if mortgage rate not found
        try {
            statement = conn.createStatement();//create a statement from the connection object
            resultset = statement.executeQuery(query); //execute the query

            while (resultset.next()) {
                //get mortgage rate from bank having id of 
                mortgageRate = resultset.getString("bank_mortgage");
                mortgageDouble = Double.parseDouble(mortgageRate);
            }
        } catch (Exception exception) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("This bank with id of " + bankId +" doesn't exist.");
            a.show();
        }
        return mortgageDouble;
    }

    /**
     * getBankId returns bankId for a bank name and creates an alert box if bank
     * with that name does not exist.
     *
     * @param bankName String
     * @return int bank id
     */
    public static int getBankId(String bankName) {
        DatabaseReferrer dbRef = DatabaseReferrer.getInstance();
        Connection conn = dbRef.returnConn();
        //create a connection from the returned connection object of the returnConn method
        String query = "SELECT bank_id FROM banks WHERE bank_name=" + "'" + bankName + "'" + "";
        Statement statement;
        ResultSet resultset;
        int bankId = 0;
        try {
            statement = conn.createStatement();//create a statement from the connection object
            resultset = statement.executeQuery(query); //execute the query
            if (resultset.next() == false) {
                throw new SQLException("Selected " + bankName + " does not exist in database !");
            } else {
                do {
                    bankId = resultset.getInt("bank_id");
                   
                } while (resultset.next());
            }
        } catch (SQLException exception) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("Add selected bank to Institutions.");
            a.show();

        }

        return bankId;

    }

    /**
     * checkIfPropertyExists checks if property with the input id exists in the
     * database and returns a String with the property address, if the property
     * with the id is not found , returns string address : "No Address Found !"
     *
     * @param propertyId
     * @return
     */
    public static String checkIfPropertyExists(int propertyId) {
        DatabaseReferrer dbRef = DatabaseReferrer.getInstance();
        Connection conn = dbRef.returnConn();
        //create a connection from the returned connection object of the returnConn method
        String query = "SELECT properties_address FROM properties WHERE properties_id=" + propertyId + "";
        Statement statement;
        ResultSet resultset;
        String propertyExistsAddr = "No Address Found !"; //Address returned by default if the property is not found
        try {
            statement = conn.createStatement();//create a statement from the connection object
            resultset = statement.executeQuery(query); //execute the query
            if (resultset.next() == false) {
                //if the property is not found, throw a new SQLException
                throw new SQLException("Selected property " + propertyId + " does not exist in database !");
            } else {
                do {
                    //if the property is found, return the address
                    propertyExistsAddr = resultset.getString("properties_address");
                
                } while (resultset.next());
            }
        } catch (SQLException exception) {
            //create an Alert error box with the message that the user needs to addd the property with this id
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("First add this property with id of : " + propertyId + " !");
            a.show();

        }
        return propertyExistsAddr;
    }

    /**
     * countBanks counts the number of banks in the database and if the number
     * is 4 throw an SQLException because the user cannot have more than 4
     * banks. He should only be able to select the banks written in the RSD.
     *
     * @return int result which is the numeber of banks
     */
    public static int countBanks() {
        DatabaseReferrer dbRef = DatabaseReferrer.getInstance();
        Connection conn = dbRef.returnConn();
        //create a connection from the returned connection object of the returnConn method
        String query = "SELECT COUNT(bank_id) as count FROM banks ";
        Statement statement;
        ResultSet resultset;
        int result = 0;
        try {
            statement = conn.createStatement();//create a statement from the connection object
            resultset = statement.executeQuery(query); //execute the query
            while (resultset.next()) {
                result = resultset.getInt(1);
                //get the number of banks in the database
                if (result == 4) {
                    throw new SQLException();
                    //if already 4 banks exist, throw an SQLException 
                }
            }

        } catch (SQLException exception) {
            //catch the exception and output an Alert box of type error 
            //with the message : "You cannot add more than 4 banks !"
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("You cannot add more than 4 banks !");
            a.show();
        }
        return result;
    }

    /**
     * getSinList returns an ObservableList<String> of Tenants SIN to be
     * available for selection in the Lease View .
     *
     * @return list ObservableList<String> of Tenants
     */
    public static ObservableList<String> getSinList() {
        DatabaseReferrer dbRef = DatabaseReferrer.getInstance();
        Connection conn = dbRef.returnConn();
        //create a connection from the returned connection object of the returnConn method
        String query = "SELECT tenants_sin FROM tenants "; //get all the tenants sin from tenants
        Statement statement;
        ResultSet resultset;
        ObservableList<String> list = FXCollections.observableArrayList();
        //create an ObservableList<String> for teanants 
        String sin;
        try {
            statement = conn.createStatement();//create a statement from the connection object
            resultset = statement.executeQuery(query); //execute the query
            while (resultset.next()) {
                sin = resultset.getString("tenants_sin");
                list.add(sin); // add the found String to the list
            }

        } catch (SQLException exception) {
            //catch the error that no leases are found and output an Alert box with the error message
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setContentText("No tenants exist in the database !");
            a.show();
        }
        return list;

    }

    /**
     * checkIfLeaseExists checks if the lease with the following sin already
     * exists in database, if it exists, output an Alert error box with the
     * error message.
     *
     * @param Sin String //which is the SIN of the tenant
     * @return boolean result // true if the lease with that sin already exists
     */
    public static boolean checkIfLeaseExists(String Sin) {
        DatabaseReferrer dbRef = DatabaseReferrer.getInstance();
        Connection conn = dbRef.returnConn();
        //create a connection from the returned connection object of the returnConn method
        String query = "SELECT leases_path FROM leases WHERE leases_sin=" + Sin + "";
        Statement statement;
        ResultSet resultset;
        boolean result = false;
        try {
            statement = conn.createStatement();//create a statement from the connection object
            resultset = statement.executeQuery(query); //execute the query
            while (resultset.next()) {
                //if the lease with that sin exitst , throw an SQLException 
                throw new SQLException("Lease with this sin already exists ");
            }
        } catch (SQLException exception) {
            result = true;
            Alert a = new Alert(Alert.AlertType.ERROR);
            //create an error Alert box with the error message 
            a.setContentText("Cannot add another lease with sin : " + Sin + " !");
            a.show();

        }
        return result;
    }
}
