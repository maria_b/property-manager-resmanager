/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Maria
 * Created: Apr. 24, 2021
 * Statements used to create table 
 */

 
 DROP TABLE contractors;
 DROP TABLE banks;
 DROP TABLE tenants;
 DROP TABLE properties;

CREATE TABLE `contractors` (
   `contractors_name` varchar(30) NOT NULL,
   `contractors_phone` varchar(10) NOT NULL,
   `contractors_company` varchar(30) NOT NULL,
   `contractors_type` varchar(15) NOT NULL,
   `contractors_price` varchar(9) DEFAULT NULL,
   `contractors_date` date DEFAULT NULL,
    PRIMARY KEY (`contractors_name`),
    UNIQUE KEY `contractors_phone_UNIQUE` (`contractors_phone`)
 );
INSERT INTO contractors VALUES ("Hugo","6178908768","painting co","Painting","40","2021-03-07");
INSERT INTO contractors VALUES ("Gina Jones","6578908708","exterminator Z","Exterminator","234.95","2021-03-03");
INSERT INTO contractors VALUES ("Liam Liam","6548908768","happy tree","Landscaping","500","2021-03-01");
INSERT INTO contractors VALUES ("Freddy","6578908728","happy walls","Renovations","289.80","2021-03-22");
INSERT INTO contractors VALUES ("Tanya","6578008768","happy toilet","Plumbing","1000.25","2021-03-31");
INSERT INTO contractors VALUES ("James","6278908768","clean and fresh","Washing","80","2021-03-19");


CREATE TABLE `banks` (
   `bank_id` int NOT NULL,
   `bank_name` varchar(10) DEFAULT NULL,
   `bank_mortgage` varchar(7) DEFAULT NULL,
    PRIMARY KEY(`bank_id`)
 );
INSERT INTO banks VALUES (2,"RBC","0.35");
INSERT INTO banks VALUES (3,"CIBC","0.45");
INSERT INTO banks VALUES (4,"TD", "0.32");
INSERT INTO banks VALUES (5,"BMO","0.56");

CREATE TABLE `properties` (
   `properties_id` int NOT NULL,
   `properties_type` varchar(10) NOT NULL,
   `properties_address` varchar(50) NOT NULL,
   `properties_totalunits` int DEFAULT NULL,
   `properties_emptyunits` int DEFAULT NULL,
   `properties_citytax` decimal(10,2) NOT NULL,
   `properties_schooltax` decimal(10,2) NOT NULL,
   `fk_properties_bankid` int DEFAULT NULL,
   `properties_mortgage` decimal(5,5) DEFAULT NULL,
   `properties_mortgagesum` decimal(7,5) DEFAULT NULL,
   `properties_loanlength` decimal(7,5) DEFAULT NULL,
   PRIMARY KEY (`properties_id`),
   FOREIGN KEY(`fk_properties_bankid`) REFERENCES banks(`bank_id`) ON DELETE CASCADE
);

CREATE TABLE `tenants` (
   `tenants_sin` varchar(9) NOT NULL,
   `tenants_houseid` int NOT NULL,
   `tenants_fullname` varchar(30) NOT NULL,
   `tenants_phone` varchar(10) NOT NULL,
   `tenants_email` varchar(45) DEFAULT NULL,
   `tenants_rent` varchar(8) DEFAULT NULL,
   `tenants_utilities` varchar(8) DEFAULT NULL,
   `tenants_address` varchar(50) DEFAULT NULL,
   `tenants_rentpaid` varchar(8) DEFAULT NULL,
   `tenants_leaserenewal` varchar(8) DEFAULT NULL,
   `tenants_overdue` varchar(8) DEFAULT NULL,
   `tenants_flag` varchar(8) DEFAULT NULL,
   PRIMARY KEY (`tenants_sin`),
   UNIQUE KEY `tenants_sin_UNIQUE` (`tenants_sin`),
   UNIQUE KEY `tenants_phone_UNIQUE` (`tenants_phone`),
   FOREIGN KEY(`tenants_houseid`) REFERENCES properties(`properties_id`) ON DELETE CASCADE
 ) ;

CREATE TABLE `leases` (
   `leases_id` int NOT NULL,
   `leases_sin` varchar(9) NOT NULL,
   `leases_path` varchar(100) NOT NULL,
   PRIMARY KEY (`leases_id`),
   FOREIGN KEY(`leases_sin`) REFERENCES tenants(`tenants_sin`) ON DELETE CASCADE
 ) ;



