/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mysqlconnection;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;

/**
 * Class responsible for the database connection
 *
 * @author Maria Barba
 * @version 1.0
 * @since 2021-04-20
 */
public class DatabaseReferrer {

    private static DatabaseReferrer instance = new DatabaseReferrer();
    private String url;
    private String user;
    private String password;
    
    /**
     * Constructor for DatabaseReferrer class, sets automatically the url 
     * connection, the user and password for mysql database
     */
    private DatabaseReferrer() {
        url = "jdbc:mysql://localhost:3306/java_project";
        user = "mar";
        password = "luv123Mar!";
    }
    /**
     * Getter for DatabaseReferrer, makes sure that the object can only be 
     * created once  
     * @return DatabaseReferrer object
     */
    public static DatabaseReferrer getInstance() {
        //if the instance has not been created, crate it
        if (instance == null) {
            instance = new DatabaseReferrer();
        }
        return instance;
    }
    /**
     * Getter for url 
     * @return String url
     */
    public String getUrl() {
        return url;
    }
    /**
     * Getter for username
     * @return String username
     */
    public String getUser() {
        return user;
    }
    /**
     * Getter for password
     * @return String password
     */
    public String getPassword() {
        return password;
    }

    /**
     * returnConn method connects to the database with the right credentials and
     * returns a Connection object.
     *
     * @return Connection
     */
    public Connection returnConn() {
        //creates Connection object
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(instance.getUrl(), instance.getUser(), instance.getPassword());
            System.out.println(connection + "Conected");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;

    }

}
