module com.mycompany.resmanager {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires java.base;
    opens com.mycompany.resmanager to javafx.fxml;
    opens com.models to javafx.base;
    exports com.mycompany.resmanager;
    

}
